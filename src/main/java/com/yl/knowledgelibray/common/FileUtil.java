package com.yl.knowledgelibray.common;

import com.yl.knowledgelibray.domain.KnowledgeBody;
import com.yl.knowledgelibray.domain.ProcessCategory;
import com.yl.knowledgelibray.domain.ProcessCategoryContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import java.awt.image.renderable.RenderableImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by luowanglin
 * Date: 2018/9/26
 * Time: 11:11
 * To change this template use File | Settings | File Templates.
 */
public class FileUtil {

    @Autowired
    private Environment env;

    public  void uploadFile(byte[] file, String filePath, String fileName) throws Exception {
        File targetFile = new File(filePath);
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }
        FileOutputStream out = new FileOutputStream(filePath+fileName);
        out.write(file);
        out.flush();
        out.close();
    }

    public void moveTotherFolders(KnowledgeBody body,String startP,String endP){
        if (body.getProcesss() != null && body.getProcesss().size() > -1) {
            for (ProcessCategory processCategory:body.getProcesss()) {
                if (processCategory.getProcessCategoryContent() != null && processCategory.getProcessCategoryContent().size() > -1) {
                    for (ProcessCategoryContent pccontent : processCategory.getProcessCategoryContent()) {
                        StringBuilder startPathStr = new StringBuilder(startP);
                        StringBuilder endPathStr = new StringBuilder(endP);
                        if (pccontent.getAttachLink() != null) {
                            String[] paths = pccontent.getAttachLink().split("/");
                            if (paths != null && paths.length > 2) {
                                startPathStr.append(paths[paths.length - 2] + File.separator);
                                startPathStr.append(paths[paths.length - 1]);
                                endPathStr.append(paths[paths.length - 2] + File.separator);
                                endPathStr.append(paths[paths.length - 1]);
                                System.out.println("**startPath:"+startPathStr.toString() + " **endPath: " + endPathStr.toString());
                                Path startPath = Paths.get(startPathStr.toString());
                                Path endPath = Paths.get(endPathStr.toString());
                                Path endPathDir = Paths.get(env.getProperty("file.upload.path")+File.separator + paths[paths.length - 2]);
                                System.out.println("endPathDir:"+endPathDir.toString());
                                if (Files.exists(endPathDir) == false) {
                                    System.out.println("文件夹不存在***");
                                    try {
                                        Files.createDirectory(endPathDir);
                                    } catch (FileAlreadyExistsException e) {
                                        // 目录已经存在
                                        System.out.println("文件已经存在...");
                                    } catch (IOException e) {
                                        // 其他发生的异常
                                        e.printStackTrace();
                                    }
                                    try {
                                        Files.copy(startPath, endPath, StandardCopyOption.REPLACE_EXISTING);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public Boolean deleteFolder(Path dirPath,Boolean deleteFolder) {
        if (Files.exists(dirPath)) {
            try {
                Files.walkFileTree(dirPath,new SimpleFileVisitor<Path>(){
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        System.out.println("delete file: " + file.toString());
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }
                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                        if (deleteFolder) {
                            Files.delete(dir);
                            System.out.println("delete dir: " + dir.toString());
                        }
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public Boolean deleteFolders(List<String> paths,String dirPath,Boolean deleteFolder) {
        Boolean isSuccess = true;
        for (String pathStr : paths) {
            if (pathStr != null) {
                String[] pathChunks = pathStr.split("/");
                if (pathChunks != null && pathChunks.length > 2) {
                    Path path = Paths.get(dirPath+File.separator+pathChunks[pathChunks.length-2]);
                    isSuccess = deleteFolder(path,true);
                }
            }
        }
        return isSuccess;
    }

}
