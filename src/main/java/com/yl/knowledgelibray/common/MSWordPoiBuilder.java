package com.yl.knowledgelibray.common;

import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.domain.*;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by luowanglin
 * Date: 2018/9/29
 * Time: 00:49
 * To change this template use File | Settings | File Templates.
 */
public class MSWordPoiBuilder implements Serializable {

    Logger logger = Logger.getLogger(this.getClass().getName());

    public MSWordPoiBuilder() {}

    /**
     * 生成Word文档
     * */
    public XWPFDocument creatmswordfile(Knowledge knowledge) {
        //新建一个文档
        XWPFDocument doc = new XWPFDocument();
        if (knowledge == null) {
            return doc;
        }
        //创建一个标题
        XWPFParagraph title = doc.createParagraph();
        title.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun titleRun = title.createRun();
        titleRun.setText(knowledge.getTitle());
        titleRun.setColor("3c3c3c");
        titleRun.setBold(true);
        titleRun.setFontFamily("Courier");
        titleRun.setFontSize(20);
        logger.log(Level.INFO, "知识："+JSON.toJSONString(knowledge));
        //Create Knowledge body
        creatKnowledge(doc,knowledge);
        if (knowledge.getAttachKnowledge() != null) {
            if (knowledge.getAttachKnowledge().size() > 0) {
                for (Knowledge knowledgePtc : knowledge.getAttachKnowledge()) {
                    creatKnowledge(doc,knowledgePtc);
                }
            }
        }
        if (knowledge.getPstcOpsKnowledges() != null) {
            if (knowledge.getPstcOpsKnowledges().size() > 0) {
                for (Knowledge knowledgeOps : knowledge.getPstcOpsKnowledges()) {
                    creatKnowledge(doc,knowledgeOps);
                }
            }
        }
        return doc;
    }

    /**
     * 关闭输出流
     * @param os
     */
    public void close(OutputStream os) {
        if (os != null) {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Knowledge Create
     * */
    private void creatKnowledge(XWPFDocument doc, Knowledge knowledge) {
        //创建头部标题
        creatPDCAHead(doc,knowledge);
        if (knowledge != null) {
            if (knowledge.getPlanContent() != null && knowledge.getPlanContent().size() > 0) {
                XWPFParagraph planT = doc.createParagraph();
                planT.setAlignment(ParagraphAlignment.LEFT);
                XWPFRun run = planT.createRun();
                run.setText("执行进程");
                run.setColor("3c3c3c");
                run.addBreak();
                run.setFontFamily("Courier");
                creatPSTCTable(doc,knowledge.getPlanContent());
            }
            if (knowledge.getProcessContent() != null && knowledge.getProcessContent().size() > 0) {
                for (ProcessCategory pc : knowledge.getProcessContent()) {
                    if (pc.getProcessCategoryContent() != null && pc.getProcessCategoryContent().size() > -1) {
                        if (pc.getProcessCategoryContent().size() > -1 && pc.getProcessCategoryContent() != null) {
                            XWPFParagraph planT = doc.createParagraph();
                            planT.setAlignment(ParagraphAlignment.LEFT);
                            XWPFRun run = planT.createRun();
                            run.setText(pc.getDescription());
                            run.setColor("3c3c3c");
                            run.addBreak();
                            run.setFontFamily("Courier");
                            creatOPSTable(doc, pc.getProcessCategoryContent());
                        }
                    }
                }
            }
        }
    }

    /**
     * PDCA Style Paragraph
     * */
    private XWPFParagraph creatPDCAHead(XWPFDocument doc,Knowledge knowledge) {
        XWPFParagraph pg = doc.createParagraph();
        pg.setAlignment(ParagraphAlignment.LEFT);
        XWPFRun run = pg.createRun();
        if (knowledge != null) {
            System.out.println("SerialNumber:"+knowledge.getSerialNumber());
            creatHeadRun(run,"标题：",knowledge.getTitle());
            creatHeadRun(run, "知识编码：", knowledge.getSerialNumber());
            creatHeadRun(run, "知识类型：", knowledge.getKnowType());
            creatHeadRun(run, "知识描述：", knowledge.getDescription());
            //PDCA
            if (knowledge.getKnowType().equals("PDCA")) {
                creatHeadRun(run, "计划：", knowledge.getPlan());
                creatHeadRun(run, "执行：", knowledge.getDoSomething());
                creatHeadRun(run, "检查：", knowledge.getCheckout());
                creatHeadRun(run, "改善行动：", knowledge.getImprovementAction());
            }else if (knowledge.getKnowType().equals("PSTC")) {
                creatHeadRun(run, "计划总负责人：", knowledge.getPlanAdmin());
                creatHeadRun(run, "最后完成期限：", knowledge.getLastTime());
                creatHeadRun(run, "验收结案目标：", knowledge.getResultSum());

            }
//        logger.log(Level.INFO,JSON.toJSONString(knowledge));
            if (knowledge.getDevices() != null && knowledge.getDevices().size() > -1) {
                StringBuilder stringBuilder = new StringBuilder();
                for (Device device : knowledge.getDevices()) {
                    stringBuilder.append(device.getDeviceName());
                    stringBuilder.append(",");
                }
                creatHeadRun(run,"关联设备：",stringBuilder.toString());
            }
        }

        return pg;
    }

    /**
     * PSTC Style Paragraph
     * */
    private XWPFTable creatPSTCTable(XWPFDocument doc,List<PSTCPlanProcess> pstcs) {
        XWPFTable table = doc.createTable(pstcs.size()+1,7);
        table.setTableAlignment(TableRowAlign.CENTER);
        tableBorderStyle(table);
        // table.set
        List<XWPFTableCell> tableCells1 = table.getRow(0).getTableCells();
        tableTextStyle(tableCells1,0,"执行步骤");
        tableTextStyle(tableCells1,1,"P-负责人员");
        tableTextStyle(tableCells1,2,"T-完成期限");
        tableTextStyle(tableCells1,3,"S-责任人完成标准、主管人验收标准");
        tableTextStyle(tableCells1,4,"向谁汇报");
        tableTextStyle(tableCells1,5,"成效验收");
        tableTextStyle(tableCells1,6,"运行维护流程");
        for (int i=1;i < pstcs.size()+1;i++) {
            List<XWPFTableCell> tableCells2 = table.getRow(i).getTableCells();
            for(int j=0;j<7;j++){
                switch (j) {
                    case 0://执行步骤
                        tableTextStyle(tableCells2.get(j),pstcs.get(i-1).getOpsSchedule());
                        break;
                    case 1://P-负责人员
                        tableTextStyle(tableCells2.get(j),pstcs.get(i-1).getpAdmin());
                        break;
                    case 2://T-完成期限
                        tableTextStyle(tableCells2.get(j),pstcs.get(i-1).gettFinishTime());
                        break;
                    case 3://S-责任人完成标准、主管人验收标准
                        tableTextStyle(tableCells2.get(j),pstcs.get(i-1).getCompleteStand());
                        break;
                    case 4://向谁汇报
                        tableTextStyle(tableCells2.get(j),pstcs.get(i-1).getRender());
                        break;
                    case 5://成效验收
                        tableTextStyle(tableCells2.get(j),pstcs.get(i-1).getResult());
                        break;
                    case 6://运行维护流程
                        tableTextStyle(tableCells2.get(j),pstcs.get(i-1).getProces());
                        break;
                        default:
                            break;
                }

            }

        }
        return table;
    }

    /**
     *General and OPS Style Paragraph
     * */
    private XWPFTable creatOPSTable(XWPFDocument doc,List<ProcessCategoryContent> pstcs) {
        XWPFTable table = doc.createTable(pstcs.size()+1,3);
        table.setTableAlignment(TableRowAlign.CENTER);
        tableBorderStyle(table);
        // table.set
        List<XWPFTableCell> tableCells1 = table.getRow(0).getTableCells();
        tableTextStyle(tableCells1,0,"序号");
        tableTextStyle(tableCells1,1,"名称");
        tableTextStyle(tableCells1,2,"关联项");
        for (int i=1;i < pstcs.size()+1;i++) {
            List<XWPFTableCell> tableCells2 = table.getRow(i).getTableCells();
            for(int j=0;j<3;j++){
                switch (j) {
                    case 0://序号
                        tableTextStyle(tableCells2.get(j),pstcs.get(i-1).getNum() + "");
                        break;
                    case 1://名称
                        tableTextStyle(tableCells2.get(j),pstcs.get(i-1).getTitle());
                        break;
                    case 2://关联项
                        tableTextStyle(tableCells2.get(j),pstcs.get(i-1).getAttachLink());
                        break;
                    default:
                        break;
                }

            }

        }
        return table;
    }

    private static void tableTextStyle(List<XWPFTableCell> tableCells1,int index,String text){
        tableTextStyle(tableCells1.get(index),text);
    }

    private static void tableBorderStyle(XWPFTable table){
        //表格属性
        CTTblPr tablePr = table.getCTTbl().addNewTblPr();
        //表格宽度
        CTTblWidth width = tablePr.addNewTblW();
        width.setW(BigInteger.valueOf(8000));
        //表格颜色
        CTTblBorders borders=table.getCTTbl().getTblPr().addNewTblBorders();
        //表格内部横向表格颜色
        CTBorder hBorder=borders.addNewInsideH();
        hBorder.setVal(STBorder.Enum.forString("single"));
        hBorder.setSz(new BigInteger("1"));
        hBorder.setColor("dddddd");
        //表格内部纵向表格颜色
        CTBorder vBorder=borders.addNewInsideV();
        vBorder.setVal(STBorder.Enum.forString("single"));
        vBorder.setSz(new BigInteger("1"));
        vBorder.setColor("dddddd");
        //表格最左边一条线的样式
        CTBorder lBorder=borders.addNewLeft();
        lBorder.setVal(STBorder.Enum.forString("single"));
        lBorder.setSz(new BigInteger("1"));
        lBorder.setColor("dddddd");
        //表格最左边一条线的样式
        CTBorder rBorder=borders.addNewRight();
        rBorder.setVal(STBorder.Enum.forString("single"));
        rBorder.setSz(new BigInteger("1"));
        rBorder.setColor("dddddd");
        //表格最上边一条线（顶部）的样式
        CTBorder tBorder=borders.addNewTop();
        tBorder.setVal(STBorder.Enum.forString("single"));
        tBorder.setSz(new BigInteger("1"));
        tBorder.setColor("dddddd");
        //表格最下边一条线（底部）的样式
        CTBorder bBorder=borders.addNewBottom();
        bBorder.setVal(STBorder.Enum.forString("single"));
        bBorder.setSz(new BigInteger("1"));
        bBorder.setColor("dddddd");
    }

    private static void tableTextStyle(XWPFTableCell tableCell,String text){
        XWPFParagraph p0 = tableCell.addParagraph();tableCell.setParagraph(p0);
        XWPFRun r0 = p0.createRun();
        // 设置字体是否加粗//
        r0.setBold(true);
        r0.setFontSize(12);
        // 设置使用何种字体
        r0.setFontFamily("Helvetica Neue");
        // 设置上下两行之间的间距
        r0.setTextPosition(12);
        r0.setColor("333333");
        r0.setText(text);
    }

    private XWPFRun creatHeadRun(XWPFRun run,String title,String value) {
        run.addBreak();
        run.setText(title);
        run.setText(value);
        return run;
    }

}
