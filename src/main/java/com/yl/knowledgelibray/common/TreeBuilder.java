package com.yl.knowledgelibray.common;

import com.yl.knowledgelibray.domain.KnowledgeType;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 11:39
 * To change this template use File | Settings | File Templates.
 */

public class TreeBuilder {

    List<KnowledgeType> nodes = new ArrayList<>();

    public List<KnowledgeType> buildTree(List<KnowledgeType> nodes) {

        TreeBuilder treeBuilder = new TreeBuilder(nodes);

        return treeBuilder.buildJSONTree();
    }

    public TreeBuilder() {
    }

    public TreeBuilder(List<KnowledgeType> nodes) {
        super();
        this.nodes = nodes;
    }

    // 构建JSON树形结构
    public List<KnowledgeType> buildJSONTree() {
        List<KnowledgeType> nodeTree = buildTree();
        return nodeTree;
    }

    // 构建树形结构
    public List<KnowledgeType> buildTree() {
        List<KnowledgeType> rootNodes = getRootNodes();
        List<KnowledgeType> notChildren = (List<KnowledgeType>) CollectionUtils.subtract(nodes, rootNodes);
        for (KnowledgeType rootNode : rootNodes) {
            findChildren(rootNode,notChildren);
        }
        return rootNodes;
    }

    // 判断是否为根节点
    public boolean rootNode(KnowledgeType node) {
        if (node.getFatherCode() == null || node.getFatherCode().equals("") || node.getFatherCode().equals(node.getTypeCode())) {
            return true;
        }else{
            return false;
        }
    }

    // 获取集合中所有的根节点
    public List<KnowledgeType> getRootNodes() {
        List<KnowledgeType> rootNodes = new ArrayList<>();
        for (KnowledgeType n : nodes) {
            if (rootNode(n)) {
                rootNodes.add(n);
            }
        }
        return rootNodes;
    }

    //查找子节点
    private List<KnowledgeType> findChildren(KnowledgeType root, List<KnowledgeType> allNodes) {
        List<KnowledgeType> children = new ArrayList<KnowledgeType>();
        for (KnowledgeType comparedOne : allNodes) {
            if (comparedOne.getFatherCode().equals(root.getTypeCode())) {
                children.add(comparedOne);
            }
        }
        root.setChildren(children);
        List<KnowledgeType> notChildren = (List<KnowledgeType>) CollectionUtils.subtract(allNodes, children);
        for (KnowledgeType child : children) {
            findChildren(child, notChildren);
        }
        return children;
    }
}
