package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.ProcessCategoryContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */
public interface ProcessCategoryContentRepository extends JpaRepository<ProcessCategoryContent,Integer> {

    List<ProcessCategoryContent> findProcessCategoryContentsByProcessIdOrderByContentId(Integer processId);

    void deleteByContentId(Integer contentId);

    void deleteAllByProcessId(Integer processId);

    ProcessCategoryContent findProcessCategoryContentsByContentId(Integer contentId);

    @Query(value = "select tb_process_category_content.attach_link from tb_process_category_content where tb_process_category_content.content_id =:contentId ",nativeQuery = true)
    String findAttachLinkByContentId(@Param("contentId") Integer contentId);

    @Query(value = "select tb_process_category_content.attach_link from tb_process_category_content where tb_process_category_content.process_id =:processId",nativeQuery = true)
    List<String> findAttachLinksByProcessId(@Param("processId") Integer processId);

}
