package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.ProcessCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */
public interface ProcessCategoryRepository extends JpaRepository<ProcessCategory,Integer> {

    List<ProcessCategory> findProcessCategoriesByKnowledgeIdOrderByProcessId(Integer knowledgeId);
    void deleteByProcessId(Integer processId);
    void deleteAllByKnowledgeId(Integer knowledgeId);
}
