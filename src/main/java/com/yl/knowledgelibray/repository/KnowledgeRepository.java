package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.Knowledge;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 20:45
 * To change this template use File | Settings | File Templates.
 */
public interface KnowledgeRepository extends JpaRepository<Knowledge,Integer> {

    @Query(value = "select tb_knowledge.* from tb_knowledge order by tb_knowledge.create_time DESC \n-- #pageable\n",
            countQuery = "SELECT count(*) FROM tb_knowledge",
            nativeQuery = true)
    Page<Knowledge> findAllKnowledgeByUserId(Pageable pageable);

    @Query(value = "select tb_knowledge.* from tb_knowledge where tb_knowledge.know_type = :knowledgeType order by tb_knowledge.create_time DESC \n-- #pageable\n",
            countQuery = "SELECT count(*) FROM tb_knowledge where know_type = :knowledgeType",
            nativeQuery = true)
    Page<Knowledge> findKnowledgeByType(@Param("knowledgeType") String knowledgeType, Pageable pageable);

    @Modifying
    @Query(value = "select tb_knowledge.* from tb_knowledge where tb_knowledge.know_type = :knowledgeType order by tb_knowledge.create_time DESC",nativeQuery = true)
    List<Knowledge> findKnowledgeByType(@Param("knowledgeType") String knowledgeType);

    void deleteKnowledgeByKnowledgeId(Integer knowledgeId);

    void deleteKnowledgesByTypeCode(Integer typeCode);

    Knowledge findKnowledgeByKnowledgeId(Integer knowledgeId);

    @Query(value = "select tb_knowledge.* from tb_knowledge where (tb_knowledge.know_type like %?1%) or (tb_knowledge.time like %?1%) or (tb_knowledge.creat_name like %?1%) or (tb_knowledge.title like %?1%) or (tb_knowledge.serial_number like %?1%) or (tb_knowledge.description like %?1%) order by ?#{#pageable}",
            countQuery = "SELECT count(*) FROM tb_knowledge where (tb_knowledge.know_type like %?1%) or (tb_knowledge.time like %?1%) or (tb_knowledge.creat_name like %?1%) or (tb_knowledge.title like %?1%) or (tb_knowledge.serial_number like %?1%) or (tb_knowledge.description like %?1%)",
            nativeQuery = true)
    Page<Knowledge> knowledgesFilterByKeyword(String keyWord, Pageable pageable);

    @Query(value = "select tb_knowledge.* from tb_knowledge where ((tb_knowledge.type_code = ?1 or tb_knowledge.type_father_code = ?1) and (tb_knowledge.know_type like %?2% or tb_knowledge.time like %?2% or tb_knowledge.creat_name like %?2% or tb_knowledge.title like %?2% or tb_knowledge.serial_number like %?2% or tb_knowledge.description like %?2%)) order by ?#{#pageable}",
            countQuery = "SELECT count(*) FROM tb_knowledge where ((tb_knowledge.type_code = ?1 or tb_knowledge.type_father_code = ?1) and (tb_knowledge.know_type like %?2% or tb_knowledge.time like %?2% or tb_knowledge.creat_name like %?2% or tb_knowledge.title like %?2% or tb_knowledge.serial_number like %?2% or tb_knowledge.description like %?2%))",
            nativeQuery = true)
    Page<Knowledge> knowledgesFilterByTypeCode(String typeCode,String keyWord, Pageable pageable);

    @Modifying
    @Query(value = "select tb_knowledge.* from tb_knowledge where (tb_knowledge.know_type like %:keyWord% or tb_knowledge.create_time like %:keyWord% or tb_knowledge.creat_name like %:keyWord% or tb_knowledge.title like %:keyWord% or tb_knowledge.serial_number like %:keyWord%)or (tb_knowledge.description like %:keyWord%) order by tb_knowledge.create_time DESC ",nativeQuery = true)
    List<Knowledge> findKnowledgeByKeyWord(@Param("keyWord") String key);

    @Query(value = "select tb_knowledge.knowledge_id from tb_knowledge where tb_knowledge.type_code =:typeCode",nativeQuery = true)
    List<Integer> findKnowledgeIdsByTypeCode(@Param("typeCode") Integer typeCode);


    List<Knowledge> findKnowledgesByTypeCode(Integer typeCode);

    @Query(value = "select tb_knowledge.title from tb_knowledge where tb_knowledge.knowledge_id =:knowledgeId",nativeQuery = true)
    String findTitleByKnowledgeId(@Param("knowledgeId") Integer knowledgeId);
}
