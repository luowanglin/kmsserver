package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/10/10
 * Time: 10:37
 * To change this template use File | Settings | File Templates.
 */
public interface DeviceRepository extends JpaRepository<Device,Integer> {

    void deleteAllByKnowledgeId(Integer knowledgeId);

    List<Device> findDevicesByKnowledgeId(Integer knowledgeId);

}
