package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.PSTCPlanProcess;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 17:24
 * To change this template use File | Settings | File Templates.
 */
public interface PSTCPlanProcessRepository extends JpaRepository<PSTCPlanProcess,Integer> {

    List<PSTCPlanProcess> findAllByKnowledgeIdOrderBySeqNum(Integer processId);

    void deleteAllByKnowledgeId(Integer knowledgeId);

    void deleteById(Integer id);

}
