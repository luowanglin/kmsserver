package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.KnowledgeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 20:59
 * To change this template use File | Settings | File Templates.
 */
public interface KnowledgeTypeRepository extends JpaRepository<KnowledgeType,Integer> {

    @Modifying
    @Query(value = "insert into tb_type(father_code,knowledge_type,modifi) values(?1,?2,?3,?4)",nativeQuery = true)
    void addType(Integer fatherCode, String knowledgeType, Boolean modifi);

    @Modifying
    @Query(value = "update tb_type set knowledge_type =:knowledgeType where type_code =:typeCode ;",nativeQuery = true)
    void updataKnowledgeType(@Param("typeCode") Integer typeCode,@Param("knowledgeType") String knowledgeType);

    @Modifying
    @Query(value="select * from tb_type order by type_code ",nativeQuery = true)
    List<KnowledgeType> findAllTypeByUserId();

    List<KnowledgeType> findAllByFatherCode(Integer fatherCode);

    void deleteByTypeCode(Integer typeCode);

    KnowledgeType findKnowledgeTypeByTypeCode(Integer typeCode);

    @Query(value = "select tb_type.root_type from tb_type where tb_type.type_code = :typeCode",nativeQuery = true)
    String findRootTypeByTypeCode(@Param("typeCode") Integer typeCode);

}
