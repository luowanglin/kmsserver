package com.yl.knowledgelibray.domain;

import javax.persistence.*;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tb_process_category_content")
public class ProcessCategoryContent {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer contentId;

    private Integer num;

    private Integer processId;

    private String processType;

    private String title;

    private String attachLink;

    public Integer getContentId() {
        return contentId;
    }

    public void setContentId(Integer contentId) {
        this.contentId = contentId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAttachLink() {
        return attachLink;
    }

    public void setAttachLink(String attachLink) {
        this.attachLink = attachLink;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ProcessCategoryContent) {
            ProcessCategoryContent pcc = (ProcessCategoryContent) obj;
            return this.contentId.equals(pcc.getContentId());
        }
        return super.equals(obj);
    }
}
