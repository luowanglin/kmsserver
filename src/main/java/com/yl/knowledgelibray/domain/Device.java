package com.yl.knowledgelibray.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by luowanglin
 * Date: 2018/10/10
 * Time: 10:27
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tb_device")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Device implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String deviceName;

    @JoinColumn(name = "knowledge_id")
    private Integer knowledgeId;

    @JoinColumn(name = "ast_code",columnDefinition = "varchar(40)")
    private String astCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Integer getKnowledgeId() {
        return knowledgeId;
    }

    public void setKnowledgeId(Integer knowledgeId) {
        this.knowledgeId = knowledgeId;
    }

    public String getAstCode() {
        return astCode;
    }

    public void setAstCode(String astCode) {
        this.astCode = astCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Device) {
            Device D = (Device) obj;
            return this.deviceName.equals(D.getDeviceName())
                    && this.id.equals(D.getId());
        }
        return super.equals(obj);
    }
}
