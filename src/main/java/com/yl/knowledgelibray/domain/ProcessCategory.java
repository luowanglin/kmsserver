package com.yl.knowledgelibray.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 16:51
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tb_process_category")
public class ProcessCategory implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer processId;

    private Integer knowledgeId;

    private String proType;

    private String description;

    @Transient
    private List<ProcessCategoryContent> processCategoryContent;

    public ProcessCategory() {}

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public Integer getKnowledgeId() {
        return knowledgeId;
    }

    public void setKnowledgeId(Integer knowledgeId) {
        this.knowledgeId = knowledgeId;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ProcessCategoryContent> getProcessCategoryContent() {
        return processCategoryContent;
    }

    public void setProcessCategoryContent(List<ProcessCategoryContent> processCategoryContent) {
        this.processCategoryContent = processCategoryContent;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ProcessCategory) {
            ProcessCategory pc = (ProcessCategory) obj;
            return this.processId.equals(pc.getProcessId());
        }
        return super.equals(obj);
    }
}
