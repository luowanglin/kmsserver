package com.yl.knowledgelibray.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 20:41
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tb_knowledge")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "knowledgeId")
public class Knowledge implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer knowledgeId;

    private String userId;

    @Column(columnDefinition = "varchar(40)")
    private String title;

    private String knowType;

    @Column(columnDefinition = "varchar(40)")
    private String serialNumber;

    private String description;

    private String plan;

    private String creatName;

    private String doSomething;

    private String checkout;

    private String improvementAction;

    private String attachId;

    private Integer typeCode;

    @Column(columnDefinition = "int(255)")
    private Integer typeFatherCode;

    private String time;

    private String planAdmin;

    private String lastTime;

    private String resultSum;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @org.hibernate.annotations.CreationTimestamp
    private Date createTime;

    @Transient
    private List<PSTCPlanProcess> planContent;

    @Transient
    private List<ProcessCategory> processContent;

    @Transient
    private List<Knowledge> attachKnowledge;

    @Transient
    private List<Knowledge> pstcOpsKnowledges;

    @Transient
    private List<Device> devices;

    public Knowledge() {}

    public Integer getKnowledgeId() {
        return knowledgeId;
    }

    public void setKnowledgeId(Integer knowledgeId) {
        this.knowledgeId = knowledgeId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKnowType() {
        return knowType;
    }

    public void setKnowType(String knowType) {
        this.knowType = knowType;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getCreatName() {
        return creatName;
    }

    public void setCreatName(String creatName) {
        this.creatName = creatName;
    }

    public String getDoSomething() {
        return doSomething;
    }

    public void setDoSomething(String doSomething) {
        this.doSomething = doSomething;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    public String getImprovementAction() {
        return improvementAction;
    }

    public void setImprovementAction(String improvementAction) {
        this.improvementAction = improvementAction;
    }

    public String getAttachId() {
        return attachId;
    }

    public void setAttachId(String attachId) {
        this.attachId = attachId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPlanAdmin() {
        return planAdmin;
    }

    public void setPlanAdmin(String planAdmin) {
        this.planAdmin = planAdmin;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    public String getResultSum() {
        return resultSum;
    }

    public void setResultSum(String resultSum) {
        this.resultSum = resultSum;
    }

    public List<PSTCPlanProcess> getPlanContent() {
        return planContent;
    }

    public void setPlanContent(List<PSTCPlanProcess> planContent) {
        this.planContent = planContent;
    }

    public List<ProcessCategory> getProcessContent() {
        return processContent;
    }

    public void setProcessContent(List<ProcessCategory> processContent) {
        this.processContent = processContent;
    }

    public List<Knowledge> getAttachKnowledge() {
        return attachKnowledge;
    }

    public void setAttachKnowledge(List<Knowledge> attachKnowledge) {
        this.attachKnowledge = attachKnowledge;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(Integer typeCode) {
        this.typeCode = typeCode;
    }

    public Integer getTypeFatherCode() {
        return typeFatherCode;
    }

    public void setTypeFatherCode(Integer typeFatherCode) {
        this.typeFatherCode = typeFatherCode;
    }

    public List<Knowledge> getPstcOpsKnowledges() {
        return pstcOpsKnowledges;
    }

    public void setPstcOpsKnowledges(List<Knowledge> pstcOpsKnowledges) {
        this.pstcOpsKnowledges = pstcOpsKnowledges;
    }
}
