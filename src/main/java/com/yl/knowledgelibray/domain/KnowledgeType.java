package com.yl.knowledgelibray.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 20:57
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tb_type")
public class KnowledgeType implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY) // MYSQL时可以这样使用自增
    private Integer typeCode;

    private String userId;

    private String knowledgeType;

    private Integer fatherCode;

    @Column(columnDefinition = "varchar(25)")
    private String rootType;

    @Column(nullable = false)
    private Boolean modifi;

    @Transient
    private List<KnowledgeType> children;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getKnowledgeType() {
        return knowledgeType;
    }

    public void setKnowledgeType(String knowledgeType) {
        this.knowledgeType = knowledgeType;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(Integer typeCode) {
        this.typeCode = typeCode;
    }

    public Integer getFatherCode() {
        return fatherCode;
    }

    public void setFatherCode(Integer fatherCode) {
        this.fatherCode = fatherCode;
    }

    public Boolean getModifi() {
        return modifi;
    }

    public void setModifi(Boolean modifi) {
        this.modifi = modifi;
    }

    public List<KnowledgeType> getChildren() {
        return children;
    }

    public void setChildren(List<KnowledgeType> children) {
        this.children = children;
    }

    public String getRootType() {
        return rootType;
    }

    public void setRootType(String rootType) {
        this.rootType = rootType;
    }
}
