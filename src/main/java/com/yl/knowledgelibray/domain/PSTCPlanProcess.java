package com.yl.knowledgelibray.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tb_pstc_plan_process")
public class PSTCPlanProcess implements Serializable {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    private Integer id;

    private Integer knowledgeId;

    private String proces;

    private String pAdmin;

    private String tFinishTime;

    private String completeStand;

    private String render;

    private String result;

    private String opsSchedule;

    private Integer seqNum;

    public PSTCPlanProcess(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id){this.id = id;}

    public Integer getKnowledgeId() {
        return knowledgeId;
    }

    public void setKnowledgeId(Integer knowledgeId) {
        this.knowledgeId = knowledgeId;
    }

    public String getProces() {
        return proces;
    }

    public void setProces(String proces) {
        this.proces = proces;
    }

    public String getpAdmin() {
        return pAdmin;
    }

    public void setpAdmin(String pAdmin) {
        this.pAdmin = pAdmin;
    }

    public String gettFinishTime() {
        return tFinishTime;
    }

    public void settFinishTime(String tFinishTime) {
        this.tFinishTime = tFinishTime;
    }

    public String getCompleteStand() {
        return completeStand;
    }

    public void setCompleteStand(String completeStand) {
        this.completeStand = completeStand;
    }

    public String getRender() {
        return render;
    }

    public void setRender(String render) {
        this.render = render;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getOpsSchedule() {
        return opsSchedule;
    }

    public void setOpsSchedule(String opsSchedule) {
        this.opsSchedule = opsSchedule;
    }

    public Integer getSeqNum() {
        return seqNum;
    }

    public void setSeqNum(Integer seqNum) {
        this.seqNum = seqNum;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PSTCPlanProcess) {
            PSTCPlanProcess P = (PSTCPlanProcess) obj;
            return this.id.equals(P.getId());
        }
        return super.equals(obj);
    }
}
