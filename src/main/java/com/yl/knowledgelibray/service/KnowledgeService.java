package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.Knowledge;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 20:47
 * To change this template use File | Settings | File Templates.
 */
public interface KnowledgeService {
     HashMap findAllKnowledge(String userId, Pageable pageable);
     HashMap findKnowledgeByType(String type, String userId, Pageable pageable);
     List<Knowledge> findKnowledgeByType(String type, String userId);
     Knowledge findKnowledgeById(Integer knowledgeId);
     HashMap findKnowledgeByKey(String keyword, String knowType, String userId, Pageable pageable);
     Knowledge addKnowledge(Knowledge knowledge);
     void deleteKnowledgeByKnowledgeId(Integer knowledgeId);
     Knowledge updataKnowledge(Knowledge knowledge);
     List<Integer> findKnowledgeIdsByTypeCode(Integer typeCode);
     List<Knowledge> findKnowledgeByTypeKnow(String type);
     String findKnowledgeTitle(Integer knowledgeId);
     HashMap findknowledgeByTypeCode(Integer typeCode, Pageable pageable);
}
