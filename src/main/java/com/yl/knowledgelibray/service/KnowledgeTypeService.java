package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.KnowledgeType;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 21:00
 * To change this template use File | Settings | File Templates.
 */
public interface KnowledgeTypeService {

     KnowledgeType addType(KnowledgeType knowledgeType);

     KnowledgeType updateType(KnowledgeType knowledgeType);

     List<KnowledgeType> findAllTypes(String userid);

     void deleteKnowledgeType(Integer typeCode);

     void deleteBatchKnowledgeType(List<Integer> typeCodes);

     KnowledgeType findKnowledgeTypeByTypeCode(Integer typeCode);

     List<Integer> findAllTypeCodeByTypeKnow(String typeKnow);

     String findRootType(Integer typeCode);

    List<Integer> findChildenCodeByTypeCode(Integer typeCode);
}
