package com.yl.knowledgelibray.service;


import com.yl.knowledgelibray.domain.Device;
import com.yl.knowledgelibray.domain.Knowledge;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/10/10
 * Time: 10:38
 * To change this template use File | Settings | File Templates.
 */
public interface DeviceService {

    void deleteAllDeviceByKnowledge(Integer knowledgeId);
    void deleteDeviceById(Integer id);
    void updateDeviceBy(Device device);
    List<Device> findDevicesByKnowledgeId(Integer knowledgeId);
    List<Device> updateAllDevices(List<Device> devices);

}
