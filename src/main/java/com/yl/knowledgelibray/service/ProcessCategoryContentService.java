package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.ProcessCategoryContent;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 20:51
 * To change this template use File | Settings | File Templates.
 */
public interface ProcessCategoryContentService {

    List<ProcessCategoryContent> findCategoryContentByProcessId(Integer processId);
    void deleteCategoryContentByContentId(Integer contentId);
    void deleteCategoryContentByProcessId(Integer processId);
    ProcessCategoryContent updataCategoryContent(ProcessCategoryContent processCategoryContent);
    List<ProcessCategoryContent> addCategoryContent(List<ProcessCategoryContent> contents);
    ProcessCategoryContent addCategoryContent(ProcessCategoryContent content);
    String findAttachLinkByContentId(Integer contentId);
    List<String> findAttachLinksByProcessId(Integer processId);
    ProcessCategoryContent findCategoryContentByContentId(Integer contentId);
    List<ProcessCategoryContent> updateAllProcessCategoryContent(List<ProcessCategoryContent> processCategoryContents);
}
