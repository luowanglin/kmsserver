package com.yl.knowledgelibray.service.Impl;

import com.yl.knowledgelibray.domain.ProcessCategory;
import com.yl.knowledgelibray.domain.ProcessCategoryContent;
import com.yl.knowledgelibray.repository.ProcessCategoryContentRepository;
import com.yl.knowledgelibray.repository.ProcessCategoryRepository;
import com.yl.knowledgelibray.service.ProcessCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/26
 * Time: 01:40
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ProcessCategoryServiceImpl implements ProcessCategoryService {

    @Autowired
    ProcessCategoryRepository processCategoryRepository;
    @Autowired
    ProcessCategoryContentServiceImpl processCategoryContentService;


    @Override
    @Transactional
    public ProcessCategory addProcessCategory(ProcessCategory processCategory) {
        return processCategoryRepository.save(processCategory);
    }

    @Override
    @Transactional
    public void deleteProcessCategoryByProcessId(Integer processId) {
        processCategoryContentService.deleteCategoryContentByProcessId(processId);
        processCategoryRepository.deleteByProcessId(processId);
    }

    @Override
    @Transactional
    public void deleteProcessCategoryBatch(Integer knowledgeId) {
        List<ProcessCategory> list = processCategoryRepository.findProcessCategoriesByKnowledgeIdOrderByProcessId(knowledgeId);
        for (ProcessCategory pc : list) {
            if (pc.getProcessId() != null) {
                processCategoryContentService.deleteCategoryContentByProcessId(pc.getProcessId());
            }
        }
        processCategoryRepository.deleteAllByKnowledgeId(knowledgeId);
    }

    @Override
    public List<ProcessCategory> findProcessCategoryByKnowledgeId(Integer knowledgeId) {
        List<ProcessCategory> list = processCategoryRepository.findProcessCategoriesByKnowledgeIdOrderByProcessId(knowledgeId);
        for (ProcessCategory pc : list) {
            List<ProcessCategoryContent> pccList = processCategoryContentService.findCategoryContentByProcessId(pc.getProcessId());
            pc.setProcessCategoryContent(pccList);
        }
        return list;
    }

    @Override
    @Transactional
    public ProcessCategory updataProcessCategory(ProcessCategory processCategory) {
        return processCategoryRepository.saveAndFlush(processCategory);
    }

    @Override
    public void updataAllProcessCategory(List<ProcessCategory> processCategories) {
        for (ProcessCategory processCategory:processCategories) {
            ProcessCategory pn = updataProcessCategory(processCategory);
            if (processCategory.getProcessCategoryContent() != null) {
                for (int i = 0; i < processCategory.getProcessCategoryContent().size();i++) {
                    ProcessCategoryContent pcc = processCategory.getProcessCategoryContent().get(i);
                    if (pcc.getProcessId() == null) {
                        pcc.setProcessId(pn.getProcessId());
                        pcc.setNum(i+1);
                    }
                }
                processCategoryContentService.updateAllProcessCategoryContent(processCategory.getProcessCategoryContent());
            }
        }

    }
}
