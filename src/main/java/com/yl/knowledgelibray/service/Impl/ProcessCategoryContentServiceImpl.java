package com.yl.knowledgelibray.service.Impl;

import com.yl.knowledgelibray.domain.ProcessCategoryContent;
import com.yl.knowledgelibray.repository.ProcessCategoryContentRepository;
import com.yl.knowledgelibray.service.ProcessCategoryContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/26
 * Time: 01:50
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ProcessCategoryContentServiceImpl implements ProcessCategoryContentService {

    @Autowired
    ProcessCategoryContentRepository pccr;

    @Override
    public List<ProcessCategoryContent> findCategoryContentByProcessId(Integer processId) {
        return pccr.findProcessCategoryContentsByProcessIdOrderByContentId(processId);
    }

    @Override
    @Transactional
    public void deleteCategoryContentByContentId(Integer contentId) {
        pccr.deleteByContentId(contentId);
    }

    @Override
    @Transactional
    public void deleteCategoryContentByProcessId(Integer processId) {
        pccr.deleteAllByProcessId(processId);
    }

    @Override
    @Transactional
    public ProcessCategoryContent updataCategoryContent(ProcessCategoryContent processCategoryContent) {
        return pccr.saveAndFlush(processCategoryContent);
    }

    @Override
    @Transactional
    public List<ProcessCategoryContent> addCategoryContent(List<ProcessCategoryContent> contents) {
        return pccr.save(contents);
    }

    @Override
    @Transactional
    public ProcessCategoryContent addCategoryContent(ProcessCategoryContent content) {
        return pccr.save(content);
    }

    @Override
    public String findAttachLinkByContentId(Integer contentId) {
        return pccr.findAttachLinkByContentId(contentId);
    }

    @Override
    public List<String> findAttachLinksByProcessId(Integer processId) {
        return pccr.findAttachLinksByProcessId(processId);
    }

    @Override
    public ProcessCategoryContent findCategoryContentByContentId(Integer contentId) {
        return pccr.findProcessCategoryContentsByContentId(contentId);
    }

    @Override
    public List<ProcessCategoryContent> updateAllProcessCategoryContent(List<ProcessCategoryContent> processCategoryContents) {
        return pccr.save(processCategoryContents);
    }

}
