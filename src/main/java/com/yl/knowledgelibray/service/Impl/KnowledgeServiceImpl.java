package com.yl.knowledgelibray.service.Impl;

import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.common.ResponseDataTool;
import com.yl.knowledgelibray.domain.Device;
import com.yl.knowledgelibray.domain.Knowledge;
import com.yl.knowledgelibray.domain.PSTCPlanProcess;
import com.yl.knowledgelibray.domain.ProcessCategory;
import com.yl.knowledgelibray.repository.KnowledgeRepository;
import com.yl.knowledgelibray.service.DeviceService;
import com.yl.knowledgelibray.service.KnowledgeService;
import com.yl.knowledgelibray.service.PSTCPlanProcessService;
import com.yl.knowledgelibray.service.ProcessCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 20:47
 * To change this template use File | Settings | File Templates.
 */
@Service
public class KnowledgeServiceImpl implements KnowledgeService {

    @Autowired
    KnowledgeRepository knowledgeRepository;
    @Autowired
    PSTCPlanProcessService pstcPlanProcessService;
    @Autowired
    ProcessCategoryService processCategoryService;
    @Autowired
    DeviceService deviceService;
    @Autowired
    KnowledgeTypeServiceImpl knowledgeTypeService;

    @Override
    public HashMap findAllKnowledge(String userId, Pageable pageable) {
        Page<Knowledge> pages = knowledgeRepository.findAllKnowledgeByUserId(pageable);
        return ResponseDataTool.getTool().returnPageableData(pageable,pages);
    }

    @Override
    public HashMap findKnowledgeByType(String type, String userId, Pageable pageable) {
        Page<Knowledge> pages = knowledgeRepository.findKnowledgeByType(type,pageable);
        return ResponseDataTool.getTool().returnPageableData(pageable,pages);
    }

    @Override
    public List<Knowledge> findKnowledgeByType(String type, String userId) {
        List<Knowledge> list = knowledgeRepository.findKnowledgeByType(type);
        return list;
    }

    @Override
    public Knowledge findKnowledgeById(Integer knowledgeId) {
        Knowledge knowledge = knowledgeRepository.findKnowledgeByKnowledgeId(knowledgeId);
        List<PSTCPlanProcess> planContents = pstcPlanProcessService.findAllPlanProcessByKnowledgeId(knowledgeId);
        List<ProcessCategory> processContents = processCategoryService.findProcessCategoryByKnowledgeId(knowledgeId);
        List<Device> devices = deviceService.findDevicesByKnowledgeId(knowledgeId);
        if (knowledge != null){
            knowledge.setPlanContent(planContents);
            knowledge.setProcessContent(processContents);
            knowledge.setDevices(devices);
        }
        return knowledge;
    }

    @Override
    public HashMap findKnowledgeByKey(String keyword,String typeCode,String userId,Pageable pageable) {
        Page<Knowledge> pages = null;
        if (keyword == null) {
            keyword = "";
        }
        if (typeCode == null || typeCode.isEmpty()) {
            pages = knowledgeRepository.knowledgesFilterByKeyword(keyword,pageable);
        }else {
//            pages = knowledgeRepository.knowledgesFilterByTypeCode(typeCode,keyword,pageable);
            HashMap hashMap = findknowledgeByTypeCode(Integer.valueOf(typeCode),pageable);
            List<Knowledge> lists = (List<Knowledge>) hashMap.get("data");
            List<Knowledge> knowledges = search(keyword,lists);
            return ResponseDataTool.getTool().returnPageable(pageable,knowledges);
        }
        return ResponseDataTool.getTool().returnPageableData(pageable,pages);
    }
    private  List search(String keyword,List<Knowledge> list){
        List<Knowledge> results = new ArrayList();
        Pattern pattern = Pattern.compile(keyword,Pattern.CASE_INSENSITIVE);
        for(Knowledge kg: list){
            Matcher mTitle = pattern.matcher(kg.getTitle());
            Matcher mType = pattern.matcher(kg.getKnowType());
            Matcher mCreatName = pattern.matcher(kg.getCreatName());
            Matcher mSerialNumber = pattern.matcher(kg.getSerialNumber());
            Matcher mDescription = pattern.matcher(kg.getDescription());
            Matcher mTime = pattern.matcher(kg.getTime());
            if(mTitle.find() || mType.find() || mCreatName.find() || mSerialNumber.find() || mDescription.find() || mTime.find()){
                results.add(kg);
            }
        }
        return results;
    }

    @Override
    @Transactional
    public Knowledge addKnowledge(Knowledge knowledge) {
        return knowledgeRepository.save(knowledge);
    }

    @Override
    @Transactional
    public void deleteKnowledgeByKnowledgeId(Integer knowledgeId) {
        knowledgeRepository.deleteKnowledgeByKnowledgeId(knowledgeId);
    }

    @Override
    @Transactional
    public Knowledge updataKnowledge(Knowledge knowledge) {
        return knowledgeRepository.save(knowledge);
    }

    @Override
    public List<Integer> findKnowledgeIdsByTypeCode(Integer typeCode) {
        return knowledgeRepository.findKnowledgeIdsByTypeCode(typeCode);
    }

    @Override
    public List<Knowledge> findKnowledgeByTypeKnow(String type) {
        List<Knowledge> list = new LinkedList<>();
        List<Integer> typeCodes = knowledgeTypeService.findAllTypeCodeByTypeKnow(type);
        for (Integer code:typeCodes) {
            List<Knowledge> knowledges = knowledgeRepository.findKnowledgesByTypeCode(code);
            if (knowledges != null) {
                list.addAll(knowledges);
            }
        }
        return list;
    }

    @Override
    public String findKnowledgeTitle(Integer knowledgeId) {
        return knowledgeRepository.findTitleByKnowledgeId(knowledgeId);
    }

    @Override
    public HashMap findknowledgeByTypeCode(Integer typeCode, Pageable pageable) {
        List<Knowledge> knowledges = new LinkedList<>();
        List<Integer> typeCodes = knowledgeTypeService.findChildenCodeByTypeCode(typeCode);
        for (Integer code: typeCodes) {
            List<Knowledge> tempList = knowledgeRepository.findKnowledgesByTypeCode(code);
            if (tempList != null) {
                knowledges.addAll(tempList);
            }
        }
        return ResponseDataTool.getTool().returnPageable(pageable,knowledges);
    }

}
