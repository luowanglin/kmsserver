package com.yl.knowledgelibray.service.Impl;

import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.domain.Knowledge;
import com.yl.knowledgelibray.domain.KnowledgeType;
import com.yl.knowledgelibray.repository.KnowledgeRepository;
import com.yl.knowledgelibray.repository.KnowledgeTypeRepository;
import com.yl.knowledgelibray.service.KnowledgeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/22
 * Time: 19:57
 * To change this template use File | Settings | File Templates.
 */
@Service
public class KnowledgeTypeServiceImpl implements KnowledgeTypeService {

    @Autowired
    KnowledgeTypeRepository knowledgeTypeRepository;

    @Autowired
    KnowledgeRepository knowledgeRepository;

    @Override
    @Transactional
    public KnowledgeType addType(KnowledgeType knowledgeType) {
        return knowledgeTypeRepository.save(knowledgeType);
    }

    @Override
    @Transactional
    public KnowledgeType updateType(KnowledgeType knowledgeType) {
        knowledgeTypeRepository.updataKnowledgeType(knowledgeType.getTypeCode(),knowledgeType.getKnowledgeType());
        return knowledgeTypeRepository.findKnowledgeTypeByTypeCode(knowledgeType.getTypeCode());
    }

    @Override
    public List<KnowledgeType> findAllTypes(String userid) {
        return knowledgeTypeRepository.findAllTypeByUserId();
    }

    @Override
    @Transactional
    public void deleteKnowledgeType(Integer typeCode) {
        List<KnowledgeType> lis = knowledgeTypeRepository.findAllByFatherCode(typeCode);
        for (KnowledgeType kt: lis) {
            knowledgeTypeRepository.deleteByTypeCode(kt.getTypeCode());
        }
        knowledgeTypeRepository.deleteByTypeCode(typeCode);
    }

    @Override
    @Transactional
    public void deleteBatchKnowledgeType(List<Integer> typeCodes) {
        for (Integer code : typeCodes) {
//            knowledgeRepository.deleteKnowledgesByTypeCode(code);
            knowledgeTypeRepository.deleteByTypeCode(code);
        }
    }

    @Override
    public KnowledgeType findKnowledgeTypeByTypeCode(Integer typeCode) {
        return knowledgeTypeRepository.findKnowledgeTypeByTypeCode(typeCode);
    }

    @Override
    public List<Integer> findAllTypeCodeByTypeKnow(String typeKnow) {
        List<KnowledgeType> list = knowledgeTypeRepository.findAll();
        List<Integer> typeCodes = new LinkedList<>();
        for (KnowledgeType kt:list) {
            if (kt.getRootType() != null) {
                if (kt.getRootType().equals(typeKnow)) {
                    typeCodes.add(kt.getTypeCode());
                }
            }
        }
        System.out.println("*TypeCodeS*:"+JSON.toJSONString(typeCodes));
        return typeCodes;
    }

    @Override
    public String findRootType(Integer typeCode) {
        return knowledgeTypeRepository.findRootTypeByTypeCode(typeCode);
    }

    @Override
    public List<Integer> findChildenCodeByTypeCode(Integer typeCode) {
        List<Integer> tree = new LinkedList<Integer>();
        tree.add(typeCode);
        getTreeKnowledge(tree,typeCode);
        //去重
        HashSet hash = new HashSet(tree);
        tree.clear();
        tree.addAll(hash);
        return tree;
    }

    private void getTreeKnowledge(List<Integer> tree, Integer typeCode) {
        List<KnowledgeType> list = knowledgeTypeRepository.findAllByFatherCode(typeCode);
        if (null != list && list.size()>0) {
            for (KnowledgeType kt: list) {
                tree.add(kt.getTypeCode());
                getTreeKnowledge(tree,kt.getTypeCode());
            }
        }
    }

}
