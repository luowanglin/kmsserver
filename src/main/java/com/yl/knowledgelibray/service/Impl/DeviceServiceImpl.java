package com.yl.knowledgelibray.service.Impl;

import com.yl.knowledgelibray.domain.Device;
import com.yl.knowledgelibray.domain.Knowledge;
import com.yl.knowledgelibray.repository.DeviceRepository;
import com.yl.knowledgelibray.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/10/10
 * Time: 10:41
 * To change this template use File | Settings | File Templates.
 */
@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    DeviceRepository deviceRepository;

    @Transactional
    @Override
    public void deleteAllDeviceByKnowledge(Integer knowledgeId) {
        deviceRepository.deleteAllByKnowledgeId(knowledgeId);
    }

    @Override
    public void deleteDeviceById(Integer id) {
        deviceRepository.delete(id);
    }

    @Transactional
    @Override
    public void updateDeviceBy(Device device) {
        deviceRepository.save(device);
    }

    @Transactional
    @Override
    public List<Device> findDevicesByKnowledgeId(Integer knowledgeId) {
        return deviceRepository.findDevicesByKnowledgeId(knowledgeId);
    }

    @Transactional
    @Override
    public List<Device> updateAllDevices(List<Device> devices) {
        return deviceRepository.save(devices);
    }

}
