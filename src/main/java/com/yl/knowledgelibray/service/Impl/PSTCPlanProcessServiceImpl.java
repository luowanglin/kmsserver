package com.yl.knowledgelibray.service.Impl;

import com.yl.knowledgelibray.domain.PSTCPlanProcess;
import com.yl.knowledgelibray.repository.PSTCPlanProcessRepository;
import com.yl.knowledgelibray.service.PSTCPlanProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/23
 * Time: 23:20
 * To change this template use File | Settings | File Templates.
 */
@Service
public class PSTCPlanProcessServiceImpl implements PSTCPlanProcessService {

    @Autowired
    PSTCPlanProcessRepository pstcPlanProcessRepository;
    @Autowired
    KnowledgeServiceImpl knowledgeService;

    @Override
    @Transactional
    public List<PSTCPlanProcess> addAPSTCPlanBatchProcess(List<PSTCPlanProcess> processes) {
        return pstcPlanProcessRepository.save(processes);
    }

    @Override
    @Transactional
    public PSTCPlanProcess addPSTCPlanProcess(PSTCPlanProcess process) {
        return pstcPlanProcessRepository.save(process);
    }

    @Override
    @Transactional
    public void updataPSTCPlanProcess(PSTCPlanProcess pstcPlanProcess) {
        pstcPlanProcessRepository.saveAndFlush(pstcPlanProcess);
    }

    @Override
    @Transactional
    public List<PSTCPlanProcess> updataPSTCPlanBatchProcess(List<PSTCPlanProcess> processes) {
        return pstcPlanProcessRepository.save(processes);
    }

    @Override
    @Transactional
    public void deletePSTCPlanProcessById(Integer id) {
        pstcPlanProcessRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deletePSTCPlanBatchProcess(Integer knowledgeId) {
        pstcPlanProcessRepository.deleteAllByKnowledgeId(knowledgeId);
    }

    @Override
    public List<PSTCPlanProcess> findAllPlanProcessByKnowledgeId(Integer knowledgeId) {
        List<PSTCPlanProcess> list = pstcPlanProcessRepository.findAllByKnowledgeIdOrderBySeqNum(knowledgeId);
//        if (list != null) {
//            for (PSTCPlanProcess pstc:list) {
//                String ops = pstc.getProces();
//                if (ops != null) {
//                    String[] opsSplit = ops.split(",");
//                    if (opsSplit != null) {
//                        StringBuilder attachOps = new StringBuilder();
//                        for (String str:opsSplit) {
//                            attachOps.append(str+",");
//                            Integer knowId = Integer.parseInt(str);
//                            String title = knowledgeService.findKnowledgeTitle(knowId);
//                            if (title == null) {
//                                title = "未知";
//                            }
//                            attachOps.append(title+";");
//                        }
//                        pstc.setProces(attachOps.toString());
//                    }
//                }
//            }
//        }
        return list;
    }
}
