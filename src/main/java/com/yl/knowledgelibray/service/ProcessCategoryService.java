package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.ProcessCategory;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 20:51
 * To change this template use File | Settings | File Templates.
 */
public interface ProcessCategoryService {

     ProcessCategory addProcessCategory(ProcessCategory processCategory);
     void deleteProcessCategoryByProcessId(Integer processId);
     void deleteProcessCategoryBatch(Integer knowledgeId);
     List<ProcessCategory> findProcessCategoryByKnowledgeId(Integer knowledgeId);
     ProcessCategory updataProcessCategory(ProcessCategory processCategory);
     void updataAllProcessCategory(List<ProcessCategory> processCategories);
}
