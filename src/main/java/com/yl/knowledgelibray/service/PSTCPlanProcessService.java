package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.PSTCPlanProcess;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 20:52
 * To change this template use File | Settings | File Templates.
 */
public interface PSTCPlanProcessService {
     List<PSTCPlanProcess> addAPSTCPlanBatchProcess(List<PSTCPlanProcess> processes);
     PSTCPlanProcess addPSTCPlanProcess(PSTCPlanProcess process);
     void updataPSTCPlanProcess(PSTCPlanProcess pstcPlanProcess);
     List<PSTCPlanProcess> updataPSTCPlanBatchProcess(List<PSTCPlanProcess> processes);
     void deletePSTCPlanProcessById(Integer id);
     void deletePSTCPlanBatchProcess(Integer knowledgeId);
     List<PSTCPlanProcess> findAllPlanProcessByKnowledgeId(Integer knowledgeId);
}
