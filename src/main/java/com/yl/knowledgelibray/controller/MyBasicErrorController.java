package com.yl.knowledgelibray.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by luowanglin
 * Date: 2018/10/27
 * Time: 09:34
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/error")
public class MyBasicErrorController implements ErrorController {

    @Override
    public String getErrorPath() {
        System.out.println("进入错误页面...");
        return "index.html";
    }

    @RequestMapping
    public String error() {
        return getErrorPath();
    }
}
