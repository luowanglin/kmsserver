package com.yl.knowledgelibray.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yl.knowledgelibray.common.*;
import com.yl.knowledgelibray.domain.*;
import com.yl.knowledgelibray.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by luowanglin
 * Date: 2018/9/22
 * Time: 18:58
 * To change this template use File | Settings | File Templates.
 */

@Api(value = "知识库Controller",tags = {"知识库操作接口"})
@RestController
@RequestMapping(value = "/api")
public class KnowledgelibrayController {

    Logger logger = Logger.getLogger(this.getClass().getName());
    public static String uploadTempDirectory = File.separator+"temp"+File.separator;

    @Autowired
    KnowledgeTypeService knowledgeTypeService;
    @Autowired
    KnowledgeService knowledgeService;
    @Autowired
    ProcessCategoryService processCategoryService;
    @Autowired
    ProcessCategoryContentService processCategoryContentService;
    @Autowired
    PSTCPlanProcessService pstcPlanProcessService;
    @Autowired
    DeviceService deviceService;
    @Autowired
    private Environment env;
    @Autowired
    private FileUtil fileUtil;


    /**
    * 获取知识类型
    * */
    @ApiOperation(value = "获取用户下的知识类型")
    @RequestMapping(value = "/types",method = RequestMethod.GET)
    public HashMap getAllKnowledgeTypes(HttpServletRequest request,@ApiParam(name="userId",value="用户id",required=false) String userId) {
        // 获取全部目录节点
        String userid = (String) request.getSession().getAttribute("userId");
        List<KnowledgeType> nodes = knowledgeTypeService.findAllTypes(userid);
        // 拼装树形json字符串
        return ResponseDataTool.getTool().returnListContent(new TreeBuilder().buildTree(nodes));
    }

    /**
     * 添加知识树类型
     * Example:{"userId":"40288347661a413201661a4159050009","knowledgeType":"测试类型","modifi":true}
     * */
    @ApiOperation(value = "添加知识类型",notes = "请以json对象的方式传参")
    @RequestMapping(value = "/addKnowledgeType",method = RequestMethod.POST)
    public HashMap addKnowledgeType(HttpServletRequest request,@ApiParam(name="knowledgeType",value="知识类型Model",required=true) @RequestBody KnowledgeType knowledgeType) {
        String userid = (String) request.getSession().getAttribute("userId");
        if (knowledgeType == null || knowledgeType.getTypeCode() != null) {
            HashMap erro = new HashMap();
            erro.put("erro","body is null Or typecode not null");
            return ResponseDataTool.getTool().returnContent(erro);
        }
        logger.log(Level.INFO,JSON.toJSONString(knowledgeType));
        knowledgeType.setUserId(userid);
        knowledgeTypeService.addType(knowledgeType);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 删除知识类型
     * Example:["40288087662b1dc901662b1f060f0000"]
     * */
    @ApiOperation(value = "删除知识类型",notes = "将会删除该类型的知识内容，以及类型，传json对象类型")
    @RequestMapping(value = "/deleteKnowledgeType",method = RequestMethod.POST)
    public HashMap deleteKnowledgeType(@ApiParam(name="typeCodes",value="typecode集合",required=true) @RequestBody ArrayList<Integer> typeCodeS) {
        logger.log(Level.INFO,"JSON:"+JSON.toJSONString(typeCodeS));
        if (typeCodeS == null) {
            HashMap erro = new HashMap();
            erro.put("erro","typeCodes is null");
            return ResponseDataTool.getTool().returnContent(erro);
        }
        if (typeCodeS != null && typeCodeS.size() > -1) {
            for (Integer typecode : typeCodeS) {
                List<Integer> knowledgeIds = knowledgeService.findKnowledgeIdsByTypeCode(typecode);
                if (knowledgeIds != null && knowledgeIds.size() > -1) {
                    for (Integer knowId : knowledgeIds) {
                        deleteKnowledgeById(knowId);
                    }
                }
            }
        }
        knowledgeTypeService.deleteBatchKnowledgeType(typeCodeS);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 修改知识类型
     * Example:{"userId":"40288347661a413201661a4159050009","knowledgeType":"测试型","modifi":true,"typeCode":"40288087662b1dc901662b2a8c900001"}
     * */
    @ApiOperation(value = "修改知识类型",notes = "typecode不能为空，传json对象类型")
    @RequestMapping(value = "/updateKnowledgeType",method = RequestMethod.PUT)
    public HashMap updateKnowledgeType(HttpServletRequest request,
                                       @ApiParam(name="knowledgeType",value="知识类型Model",required=true) @RequestBody KnowledgeType knowledgeType) {
        String userid = (String) request.getSession().getAttribute("userId");
        if (knowledgeType != null) {
            if (knowledgeType.getTypeCode() == null || knowledgeType.getTypeCode().equals("")) {
                HashMap erro = new HashMap();
                erro.put("error","typeCode is null");
                return ResponseDataTool.getTool().returnContent(erro);
            }
            logger.log(Level.INFO,JSON.toJSONString(knowledgeType));
            knowledgeType.setUserId(userid);
            return ResponseDataTool.getTool().returnContent(knowledgeTypeService.updateType(knowledgeType));
        }
        return ResponseDataTool.getTool().returnContent(false);
    }

    /**
     * 查询类型归属
     * */
    @ApiOperation(value = "查询知识类型归属")
    @RequestMapping(value = "/findKnowledgeType",method = RequestMethod.GET)
    public HashMap findRootTypeByTypeCode(Integer typeCode) {
        String kt = knowledgeTypeService.findRootType(typeCode);
        logger.log(Level.INFO,kt);
        return ResponseDataTool.getTool().returnContent(kt);
    }

    /**
    * 获取所有知识条目
    * */
    @ApiOperation(value = "获取用户下的所有知识",notes = "userID 由传过来的token生成，分页下标从1开始")
    @RequestMapping(value = "/knowledge",method = RequestMethod.GET)
    public HashMap getAllKnowledge(HttpServletRequest request,
                                   @ApiParam(name="userId",value="用户id",required=false)String userId,
                                   @ApiParam(name="indexPage",value="分页下标",required=true)String indexPage,
                                   @ApiParam(name="pageSize",value="单页查询数量",required=true)String pageSize) {
        if (indexPage == null || pageSize == null || indexPage.isEmpty() || pageSize.isEmpty()) {
            indexPage = "1";
            pageSize = "50";
        }
        String userid = (String) request.getSession().getAttribute("userId");
        Pageable pageable = new PageRequest(Integer.parseInt(indexPage)-1,Integer.parseInt(pageSize));
        return knowledgeService.findAllKnowledge(userid,pageable);
    }

    /**
     * 树查询知识条目
     * */
    @ApiOperation(value = "通过树类型Code，获取所有父、子节点的知识条目",notes = "userID 由传过来的token生成，分页下标从1开始")
    @RequestMapping(value = "/knowledgeByTypeCode",method = RequestMethod.GET)
    public HashMap getKnowledgeByTypecode(HttpServletRequest request,
                                          @ApiParam(name="userId",value="用户id",required=false)String userId,
                                          @ApiParam(name="typeCode",value="知识Code",required=true)Integer typeCode,
                                          @ApiParam(name="indexPage",value="分页下标",required=true)String indexPage,
                                          @ApiParam(name="pageSize",value="单页查询数量",required=true)String pageSize) {
        if (indexPage == null || pageSize == null || indexPage.isEmpty() || pageSize.isEmpty()) {
            indexPage = "1";
            pageSize = "50";
        }
        String userid = (String) request.getSession().getAttribute("userId");
        Pageable pageable = new PageRequest(Integer.parseInt(indexPage)-1,Integer.parseInt(pageSize));
        return knowledgeService.findknowledgeByTypeCode(typeCode,pageable);
    }

    /**
    * 添加知识条目
    * */
    @ApiOperation(value = "新增知识内容",notes = "userID 由传过来的token生成，请以json对象的方式传参")
    @RequestMapping(value = "/addKnowledge",method = RequestMethod.POST)
    public HashMap addKnowledge(HttpServletRequest request,
                                @ApiParam(name="body",value="KnowledgeBody模型对象",required=true) @RequestBody KnowledgeBody body) {
        HttpSession session = request.getSession();
        String userid = (String) session.getAttribute("userId");
        String username = (String) session.getAttribute("userName");
        if (body == null) {
            HashMap erro = new HashMap();
            erro.put("erro","request playload json syntax error");
            return ResponseDataTool.getTool().returnHashMapContent(erro);
        }
        logger.log(Level.WARNING,"添加："+JSON.toJSONString(body));
        if ((body.getKnowledge() == null) || (body.getKnowledge().getTypeCode() == null) || (body.getKnowledge().getTypeCode().equals(""))) {
            HashMap erro = new HashMap();
            erro.put("erro","request playload knowledge json syntax error or TypeCode is null");
            return ResponseDataTool.getTool().returnHashMapContent(erro);
        }
        body.getKnowledge().setUserId(userid);
        body.getKnowledge().setCreatName(username);
        Knowledge knowledge = knowledgeService.addKnowledge(body.getKnowledge());
        //绑定设备
        if ((body.getKnowledge() != null) && (body.getKnowledge().getDevices() != null) && (body.getKnowledge().getDevices().size() > 0)) {
            for (Device device : body.getKnowledge().getDevices()) {
                device.setKnowledgeId(knowledge.getKnowledgeId());
                deviceService.updateDeviceBy(device);
            }
        }
        //保存计划列表
        if (body.getPlans() != null && body.getPlans().size() > 0) {
            for (PSTCPlanProcess ps:body.getPlans()) {
                ps.setKnowledgeId(knowledge.getKnowledgeId());
            }
            pstcPlanProcessService.addAPSTCPlanBatchProcess(body.getPlans());
        }
        //保存运维分类
        if (body.getProcesss() != null && body.getProcesss().size() > 0) {
            for (ProcessCategory pc: body.getProcesss()) {
                pc.setKnowledgeId(knowledge.getKnowledgeId());
                ProcessCategory pca = processCategoryService.addProcessCategory(pc);
                for (int i=0;i < pc.getProcessCategoryContent().size();i++) {
                    ProcessCategoryContent pcc = pc.getProcessCategoryContent().get(i);
                    pcc.setProcessId(pca.getProcessId());
                    pcc.setNum(i+1);
                }
                List<ProcessCategoryContent> list = processCategoryContentService.addCategoryContent(pca.getProcessCategoryContent());
//                for(ProcessCategoryContent pcc : list) {
//                    KnowledgeFile kf = new KnowledgeFile();
//                    kf.setContentId(pcc.getContentId());
//                    kf.setFileName(pcc.getAttachLink());
//                    knowledgeFileService.addFile(kf);
//                }
            }
        }
        //将上传的附件从临时文件中移出
        fileUtil.moveTotherFolders(body,env.getProperty("file.upload.path")+uploadTempDirectory,env.getProperty("file.upload.path")+File.separator);
        return ResponseDataTool.getTool().returnContent(knowledge);
    }

    /**
     * 获取知识条目
     * */
    @ApiOperation(value = "获取知识详情")
    @RequestMapping(value = "/getKnowledgeContent",method = RequestMethod.GET)
    public HashMap getKnowledgeContent(@ApiParam(name="knowledgeId",value="知识id",required=true) Integer knowledgeId) {
        Knowledge knowledge = knowledgeService.findKnowledgeById(knowledgeId);
        if (knowledge == null) {
            HashMap error = new HashMap();
            error.put("data",null);
            return ResponseDataTool.getTool().returnContent(error);
        }
        if (knowledge.getAttachId() != null && knowledge.getAttachId().isEmpty() == false) {
            String attachIdStr = knowledge.getAttachId();
            if (attachIdStr != null) {
                String[] attachIds = attachIdStr.split(",");
                List<Knowledge> plans = new LinkedList<>();
                for (String id : attachIds) {
                    Knowledge li = knowledgeService.findKnowledgeById(Integer.valueOf(id));
                    if (li != null) {
                        li.setPlanContent(pstcPlanProcessService.findAllPlanProcessByKnowledgeId(Integer.valueOf(id)));
                    }
                    plans.add(li);
                }
                knowledge.setAttachKnowledge(plans);
            }
        }
        logger.log(Level.WARNING,JSON.toJSONString(knowledge));
        return ResponseDataTool.getTool().returnContent(knowledge);
    }

    /**
    * 删除知识条目
    * */
    @ApiOperation(value = "删除单条知识")
    @RequestMapping(value = "/deleteKnowledge",method = RequestMethod.DELETE)
    public HashMap deleteKnowledgeById(@ApiParam(name="knowledgeId",value="知识id",required=true) Integer knowledgeId) {
        List<ProcessCategory> pca = processCategoryService.findProcessCategoryByKnowledgeId(knowledgeId);
        List<String> attachLinks = new LinkedList<>();
        for (ProcessCategory pr: pca) {
            List<String> attachs = processCategoryContentService.findAttachLinksByProcessId(pr.getProcessId());
            if (attachs != null && attachs.size() > -1) {
                attachLinks.addAll(attachs);
            }
            logger.log(Level.INFO,"attachLinks:"+JSON.toJSONString(attachLinks));
            processCategoryContentService.deleteCategoryContentByProcessId(pr.getProcessId());
        }
        //移除对应知识的所有附件
        fileUtil.deleteFolders(attachLinks,env.getProperty("file.upload.path"),true);
        deviceService.deleteAllDeviceByKnowledge(knowledgeId);
        processCategoryService.deleteProcessCategoryBatch(knowledgeId);
        pstcPlanProcessService.deletePSTCPlanBatchProcess(knowledgeId);
        knowledgeService.deleteKnowledgeByKnowledgeId(knowledgeId);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
    * 修改知识条目
    * */
    @ApiOperation(value = "修改知识详情",notes = "需要传knowledgeId,请以json对象的方式传参")
    @RequestMapping(value = "/updataKnowledge",method = RequestMethod.POST)
    public HashMap updataKnowledge(HttpServletRequest request,
                                   @ApiParam(name="body",value="KnowledgeBody json对象",required=true) @RequestBody KnowledgeBody body) {
        HttpSession session = request.getSession();
        String userid = (String) session.getAttribute("userId");
        String username = (String) session.getAttribute("userName");
        if (body == null) {
            HashMap erro = new HashMap();
            erro.put("erro","request playload json syntax error");
            return ResponseDataTool.getTool().returnHashMapContent(erro);
        }
        if ((body.getKnowledge() == null) || (body.getKnowledge().getTypeCode() == null) || (body.getKnowledge().getKnowledgeId() == null) || (body.getKnowledge().getKnowledgeId().equals(""))) {
            HashMap erro = new HashMap();
            erro.put("error","request playload knowledge json syntax error or knowledgeId is null");
            return ResponseDataTool.getTool().returnContent(erro);
        }
        logger.log(Level.INFO,"Body:"+JSON.toJSONString(body));
        body.getKnowledge().setUserId(userid);
        body.getKnowledge().setCreatName(username);

        Knowledge knowledgeOrigin = knowledgeService.findKnowledgeById(body.getKnowledge().getKnowledgeId());
        //更新设备绑定
        if ((body.getKnowledge() != null) && (body.getKnowledge().getDevices() != null) && (body.getKnowledge().getDevices().size() > 0)) {
            if ((knowledgeOrigin != null) && (knowledgeOrigin.getDevices() != null)) {
                for (Device dv : knowledgeOrigin.getDevices()) {
                    if (!body.getKnowledge().getDevices().contains(dv)) {
                        logger.log(Level.WARNING, "不包含..." + dv.getDeviceName());
                        deviceService.deleteDeviceById(dv.getId());
                    }
                }
            }
            //批量更新,关联knowledgeId外键
            for (Device device : body.getKnowledge().getDevices()) {
                device.setKnowledgeId(body.getKnowledge().getKnowledgeId());
            }
            deviceService.updateAllDevices(body.getKnowledge().getDevices());
        }
        //更新计划列表
        if (body.getPlans() != null && body.getPlans().size() > -1){
            if ((knowledgeOrigin != null) && (knowledgeOrigin.getPlanContent() != null)) {
                for (PSTCPlanProcess pp : knowledgeOrigin.getPlanContent()) {
                    if (!body.getPlans().contains(pp)) {
                        logger.log(Level.WARNING, "pstc不包含..." + pp.getpAdmin());
                        pstcPlanProcessService.deletePSTCPlanProcessById(pp.getId());
                    }
                }
            }
            for (PSTCPlanProcess ppp: body.getPlans()) {
                if (ppp.getKnowledgeId() == null) {
                    ppp.setKnowledgeId(body.getKnowledge().getKnowledgeId());
                }
            }
            pstcPlanProcessService.updataPSTCPlanBatchProcess(body.getPlans());
        }
        //更新运行维护分类
        if (body.getProcesss() != null && body.getProcesss().size() > -1){
            if ((knowledgeOrigin != null) && (knowledgeOrigin.getProcessContent() != null)) {
                for (ProcessCategory pc : knowledgeOrigin.getProcessContent()) {
                    if (!body.getProcesss().contains(pc)) {
                        logger.log(Level.WARNING, "pstc不包含..." + pc.getProType());
                        processCategoryService.deleteProcessCategoryByProcessId(pc.getProcessId());
                    }else{
                        if (pc.getProcessCategoryContent() != null) {
                            for (ProcessCategoryContent pcc : pc.getProcessCategoryContent()) {
                                for (ProcessCategory pcO : body.getProcesss()) {
                                    if (pcO.getProcessId() != null && pc.getProcessId() != null) {
                                        if (pcO.getProcessId().equals(pc.getProcessId())) {
                                            if (!pcO.getProcessCategoryContent().contains(pcc)) {
                                                processCategoryContentService.deleteCategoryContentByContentId(pcc.getContentId());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            for (ProcessCategory pc: body.getProcesss()) {
                if (pc.getKnowledgeId() == null) {
                    pc.setKnowledgeId(body.getKnowledge().getKnowledgeId());
                }
            }
            processCategoryService.updataAllProcessCategory(body.getProcesss());
        }
        Knowledge knowledge = null;
        if (body.getKnowledge().getTypeCode() != null && body.getKnowledge().getKnowledgeId() != null) {
            knowledge = knowledgeService.updataKnowledge(body.getKnowledge());
        }
        //将上传的附件从临时文件中移出
        fileUtil.moveTotherFolders(body,env.getProperty("file.upload.path")+uploadTempDirectory,env.getProperty("file.upload.path")+File.separator);
        return ResponseDataTool.getTool().returnContent(knowledge);
    }

    /**
     * 查询用户下的所有计划
     * */
    @ApiOperation(value = "查询用户下的所有PSTC类型",notes = "userID 由传过来的token生成")
    @RequestMapping(value = "/findAllPlans",method = RequestMethod.GET)
    public HashMap findAllPlans(HttpServletRequest request,
                                @ApiParam(name="userId",value="用户id",required=false) String userId) {
        String userid = (String) request.getSession().getAttribute("userId");
        return ResponseDataTool.getTool().returnContent(knowledgeService.findKnowledgeByTypeKnow("PSTC"));
    }

    /**
     * 查询用户下的所有指定类型的知识
     * */
    @ApiOperation(value = "查询用户下的所有PSTC类型",notes = "userID 由传过来的token生成")
    @RequestMapping(value = "/findAllKnowledgesByKnowType",method = RequestMethod.GET)
    public HashMap findAllKnowledgeByKnowType(HttpServletRequest request,
                                @ApiParam(name="userId",value="用户id",required=false) String userId,
                                @ApiParam(name="knowType",value="知识类型",required = true) String knowType) {
//        String userid = (String) request.getSession().getAttribute("userId");
        if (knowType == null || knowType.isEmpty()) {
            return ResponseDataTool.getTool().returnContent(false);
        }
        return ResponseDataTool.getTool().returnContent(knowledgeService.findKnowledgeByTypeKnow(knowType));
    }

    /**
     * 获取计划内容
     * */
    @ApiOperation(value = "查询PSTC内容列表")
    @RequestMapping(value = "/findPlanContent",method = RequestMethod.GET)
    public HashMap findPlanContent(
            @ApiParam(name="knowledgeId",value="知识id",required=true) Integer knowledgeId) {
        List<PSTCPlanProcess> list = pstcPlanProcessService.findAllPlanProcessByKnowledgeId(knowledgeId);
        return ResponseDataTool.getTool().returnContent(list);
    }

    /**
     * 更新计划内容
     * */
    @ApiOperation(value = "修改PSTC内容",notes = "请以json array的方式传参")
    @RequestMapping(value = "/updataPlanContent",method = RequestMethod.PUT)
    public HashMap updatePlanContent(@ApiParam(name="list",value="PSTCPlanProcess json对象类型",required=true) @RequestBody List<PSTCPlanProcess> list){
        logger.log(Level.WARNING,JSON.toJSONString(list));
        if (list == null) {
            HashMap erro = new HashMap();
            erro.put("erro","request playload json syntax error");
            return ResponseDataTool.getTool().returnHashMapContent(erro);
        }
        return ResponseDataTool.getTool().returnContent(pstcPlanProcessService.updataPSTCPlanBatchProcess(list));
    }

    /**
     * 新增运维分类
     * @param description 可传空
     * */
    @ApiOperation(value = "新增运维分类")
    @RequestMapping(value = "/addCategory",method = RequestMethod.PUT)
    public HashMap addOpsCategory(
            @ApiParam(name="description",value="分类描述",required=true) String description,
            @ApiParam(name="proType",value="分类类型",required=true) String proType,
            @ApiParam(name="knowledgeId",value="知识id",required=true) Integer knowledgeId) {
        if (knowledgeId == null) {
            HashMap erro = new HashMap();
            erro.put("erro","knowledgeId is null");
            return ResponseDataTool.getTool().returnHashMapContent(erro);
        }
        ProcessCategory processCategory = new ProcessCategory();
        processCategory.setDescription(description);
        processCategory.setProType(proType);
        processCategory.setKnowledgeId(knowledgeId);
        ProcessCategory entity = processCategoryService.addProcessCategory(processCategory);
        return ResponseDataTool.getTool().returnContent(entity);
    }

    /**
     * 更新运维分类
     * */
    @ApiOperation(value = "更新运维分类")
    @RequestMapping(value = "/updateCategory",method = RequestMethod.PUT)
    public HashMap updataOpsCategory(@ApiParam(name="proccessId",value="分类ID",required=true) Integer proccessId,
                                     @ApiParam(name="knowledgeId",value="知识ID",required=true) Integer knowledgeId,
                                     @ApiParam(name="proType",value="分类类型",required=true) String proType,
                                     @ApiParam(name="description",value="分类描述",required=true) String description) {
        if (proccessId == null || knowledgeId == null) {
            HashMap data = new HashMap();
            data.put("message","processId or knowledgeId is null");
            return ResponseDataTool.getTool().returnHashMapContent(data);
        }
        ProcessCategory processCategory = new ProcessCategory();
        processCategory.setDescription(description);
        processCategory.setProType(proType);
        processCategory.setKnowledgeId(knowledgeId);
        processCategory.setProcessId(proccessId);
        processCategoryService.updataProcessCategory(processCategory);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 查找所有运维分类
     * */
    @ApiOperation(value = "查找所有运维分类")
    @RequestMapping(value = "/findAllCategory",method = RequestMethod.GET)
    public HashMap findAllOpsCategory( @ApiParam(name="knowledgeId",value="知识id",required=true) Integer knowledgeId) {
        List<ProcessCategory> data = processCategoryService.findProcessCategoryByKnowledgeId(knowledgeId);
        return ResponseDataTool.getTool().returnListContent(data);
    }

    /**
     * 删除运维分类
     * */
    @ApiOperation(value = "删除运维分类")
    @RequestMapping(value = "/deleteCategory",method = RequestMethod.DELETE)
    public HashMap deleteOpsCategory(@ApiParam(name="process_id",value="流程id",required=true) Integer process_id) {
        processCategoryService.deleteProcessCategoryByProcessId(process_id);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 添加运维内容
     * */
    @ApiOperation(value = "添加运维内容")
    @RequestMapping(value = "/addCategoryContent",method = RequestMethod.POST)
    public HashMap addCategoryContent(@ApiParam(name="num",value="序号",required=true) Integer num,
                                      @ApiParam(name="processId",value="流程id",required=true) Integer processId,
                                      @ApiParam(name="processType",value="流程类型",required=true) String processType,
                                      @ApiParam(name="title",value="标题",required=true) String title,
                                      @ApiParam(name="attachLink",value="附件链接地址",required=true) String attachLink) {
        ProcessCategoryContent processCategoryContent = new ProcessCategoryContent();
        processCategoryContent.setNum(num);
        processCategoryContent.setProcessId(processId);
        processCategoryContent.setProcessType(processType);
        processCategoryContent.setTitle(title);
        processCategoryContent.setAttachLink(attachLink);
        ProcessCategoryContent entity = processCategoryContentService.addCategoryContent(processCategoryContent);
        return ResponseDataTool.getTool().returnContent(entity);
    }

    /**
     * 批量添加运维内容
     * */
    @ApiOperation(value = "批量添加运维内容")
    @RequestMapping(value = "/addBatchCategoryContent",method = RequestMethod.POST)
    public HashMap addBatchCategoryContent(@ApiParam(name="contents",value="ProcessCategoryContent json对象集合",required=true) List<ProcessCategoryContent> contents) {
        List<ProcessCategoryContent> lists = processCategoryContentService.addCategoryContent(contents);
        return ResponseDataTool.getTool().returnContent(lists);
    }

    /**
     * 查找运维内容
     * */
    @ApiOperation(value = "查找运维内容")
    @RequestMapping(value = "/findCategoryContetn",method = RequestMethod.GET)
    public HashMap findCategoryContetn(@ApiParam(name="processId",value="流程id",required=true) Integer processId) {
        return ResponseDataTool.getTool().returnListContent(processCategoryContentService.findCategoryContentByProcessId(processId));
    }

    /**
     * 删除运维内容
     * */
    @ApiOperation(value = "删除运维内容")
    @RequestMapping(value = "/deleteCategoryContent",method = RequestMethod.DELETE)
    public HashMap deleteCategoryContent(@ApiParam(name="contentId",value="流程内容id",required=true) Integer contentId) {
        processCategoryContentService.deleteCategoryContentByContentId(contentId);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 更新运维内容
     * */
    @ApiOperation(value = "更新运维内容")
    @RequestMapping(value = "/updataCategoryContent",method = RequestMethod.POST)
    public HashMap updataCategoryContent(@ApiParam(name="contentId",value="流程列表cell Id",required=true) Integer contentId,
                                         @ApiParam(name="num",value="序号",required=true) Integer num,
                                         @ApiParam(name="processId",value="流程Id",required=true) Integer processId,
                                         @ApiParam(name="processType",value="流程类型",required=true) String processType,
                                         @ApiParam(name="title",value="标题",required=true) String title,
                                         @ApiParam(name="attachLink",value="附件链接地址",required=true) String attachLink) {
        if (contentId == null || contentId.equals("") || processId == null || processId.equals("")) {
            HashMap data = new HashMap();
            data.put("error","contentId or processId is null");
            return ResponseDataTool.getTool().returnHashMapContent(data);
        }
        ProcessCategoryContent pcc = new ProcessCategoryContent();
        pcc.setContentId(contentId);
        pcc.setNum(num);
        pcc.setProcessId(processId);
        pcc.setProcessType(processType);
        pcc.setTitle(title);
        pcc.setAttachLink(attachLink);
        ProcessCategoryContent entity = processCategoryContentService.updataCategoryContent(pcc);
        return ResponseDataTool.getTool().returnContent(entity);
    }

    /**
     * 文件上传
     * */
    @ApiOperation(value = "文件上传",notes = "请以fromdata 表单的形式上传")
    @RequestMapping(value = "/uploadFile",method = RequestMethod.POST)
    public HashMap uploadFile(@ApiParam(name="file",value="MultipartFile",required=true) @RequestParam("file") MultipartFile file,
                           HttpServletRequest request) {
        if (file == null) {
            HashMap erro = new HashMap();
            erro.put("erro","file is null");
            return ResponseDataTool.getTool().returnHashMapContent(erro);
        }
        String fileName = file.getOriginalFilename().replaceAll("\\s*", "");
        StringBuilder uplodaPath = new StringBuilder(uploadTempDirectory);
        uplodaPath.insert(0,env.getProperty("file.upload.path"));
        logger.log(Level.INFO,"文件上传路径："+uplodaPath.toString());
        String uuid = UUID.randomUUID().toString().replace("-", "");
        uplodaPath.append(uuid+"/");
        try {
            fileUtil.uploadFile(file.getBytes(), uplodaPath.toString(), fileName);
        } catch (Exception e) {
            // TODO: handle exception
        }
        //返回json
        HashMap data = new HashMap();
        logger.log(Level.INFO,uplodaPath.toString());
        String fileUrl = "/"+ uuid +"/"+ fileName;
        data.put("data",fileUrl);
        return ResponseDataTool.getTool().returnHashMapContent(data);
    }

    /**
     * 删除文件
     * */
    @ApiOperation(value = "删除文件")
    @RequestMapping(value = "/deleteFile",method = RequestMethod.DELETE)
    public HashMap deleteFile(HttpServletRequest request,
                              @ApiParam(name="contentId",value="分类列表cell Id",required=true) String contentId) {
        if (request == null) {
            return ResponseDataTool.getTool().returnContent(false);
        }
        Boolean isDelete = false;
        String attachLink = processCategoryContentService.findAttachLinkByContentId(Integer.parseInt(contentId));
        List<String> paths = new LinkedList<>();
        if (attachLink != null) {
            paths.add(attachLink);
            isDelete = fileUtil.deleteFolders(paths,env.getProperty("file.upload.path"),true);
        }
        return ResponseDataTool.getTool().returnContent(isDelete);
    }

    /**
     * Word文件生成下载
     * */
    @ApiOperation(value = "Word文件生成下载")
    @RequestMapping(value = "/genWord",method = RequestMethod.GET)
    public void getWordFile(HttpServletResponse response, HttpServletRequest request,
                            @ApiParam(name="knowledgeId",value="知识Id",required=true) Integer knowledgeId) throws IOException {
        logger.log(Level.INFO,"prepa create Word file........."+knowledgeId);
         Knowledge knowledge = knowledgeService.findKnowledgeById(knowledgeId);
         String fileName = null;
         if (knowledge != null) {
             fileName = knowledge.getTitle();
             //Get attach knowledge for pdca
            if (knowledge.getAttachId() != null && knowledge.getAttachId().isEmpty() == false) {
                String attachIdStr = knowledge.getAttachId();
                String[] attachIds = attachIdStr.split(",");
                List<Knowledge> plans = new LinkedList<>();
                List<Knowledge> process = new LinkedList<>();
                if (attachIds != null) {
                    for (String id : attachIds) {
                        Knowledge li = knowledgeService.findKnowledgeById(Integer.valueOf(id));
                        if (li != null) {
                            li.setPlanContent(pstcPlanProcessService.findAllPlanProcessByKnowledgeId(Integer.valueOf(id)));
                            //Get Ops knowledge for pstc
                            for (PSTCPlanProcess pp:li.getPlanContent()) {
                                if (pp.getProces() != null && pp.getProces().equals("") == false) {
                                    String[] opsInfos = pp.getProces().split("/");
                                    if (opsInfos != null && opsInfos.length > 0) {
                                        for (String opsIn:opsInfos) {
                                            String[] opsKnowledges = opsIn.split(",");
                                            if (opsKnowledges != null && opsKnowledges.length > 0) {
                                                Knowledge opsK = knowledgeService.findKnowledgeById(Integer.parseInt(opsKnowledges[0]));
                                                if (opsK != null) {
                                                    opsK.setProcessContent(processCategoryService.findProcessCategoryByKnowledgeId(Integer.parseInt(opsKnowledges[0])));
                                                    process.add(opsK);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            plans.add(li);
                        }
                    }
                }
                knowledge.setAttachKnowledge(plans);
                knowledge.setPstcOpsKnowledges(process);
            }
         }
         String filePath = request.getServletContext().getRealPath("/") + "template";
         File fil= File.createTempFile("temp",".docx");
         logger.log(Level.INFO,fil.getAbsolutePath() + "~~~" + fil.getCanonicalPath() + "~~~" + fil.getPath());
        if(!fil.exists()){
            logger.log(Level.WARNING,"文件不存在。。。");
         try {
                fil.createNewFile();
            } catch (IOException e){
              e.printStackTrace();
            }
        }
        MSWordPoiBuilder builder = new MSWordPoiBuilder();
        XWPFDocument doc = builder.creatmswordfile(knowledge);
        //写出文档
        OutputStream os = null;
        try {
            os = new FileOutputStream(fil);
        } catch (FileNotFoundException e) {
            logger.log(Level.WARNING,"wenjian 没有发现...");
            e.printStackTrace();
        }
        //把doc输出到输出流
        try {
            doc.write(os);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //获得请求文件名
         System.out.println(filePath);
         //以下载方式打开
         response.setHeader("Content-Disposition", "attachment;filename="+URLEncoder.encode((fileName == null) ? "Word":fileName, "UTF-8")+".docx");
         response.setContentType("multipart/form-data");
         FileInputStream fis =  new FileInputStream(fil);
         //写出
         ServletOutputStream out = response.getOutputStream();
         //定义读取缓冲区
         byte buffer[] = new byte[1024];
         //定义读取长度
         int len = 1024;
         //循环读取
         while((len = fis.read(buffer))!=-1){
             out.write(buffer,0,len);
         }
         //释放资源
         fis.close();
         os.flush();
         os.close();
         out.flush();
         out.close();
         fil.delete();
         builder.close(os);
    }

    /**
     * 关联附件下载
     * */
    @RequestMapping(value = "/download",method = RequestMethod.GET)
    public HashMap downloadAttachFile(HttpServletResponse response,Integer contentId) {
        String filePath = processCategoryContentService.findAttachLinkByContentId(contentId);
        if (filePath != null) {
            DataInputStream dis = null;
            OutputStream os = null;
            try {
                String[] fileSplit = filePath.split("/");
                String fileName = fileSplit[fileSplit.length-1];
                logger.log(Level.WARNING,"File path:"+JSON.toJSONString(fileSplit));
                os = response.getOutputStream();
                response.setContentType("application/x-msdownload;");
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Content-disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8"));
                response.setContentType("multipart/form-data");
                StringBuilder filePathStr = new StringBuilder(env.getProperty("file.upload.path"));
                filePathStr.append(filePath);
                Path path = Paths.get(filePathStr.toString());
                File file = new File(path.toString());
                if (file != null){
                    dis = new DataInputStream(new FileInputStream(file));
                    byte[] buffer = new byte[204800];
                    int readSize = 0;
                    while ((readSize = dis.read(buffer)) != -1) {
                        os.write(buffer, 0, readSize);
                    }
                }else {
                    logger.log(Level.WARNING,"file not find");
                }
                os.flush();
                dis.close();
                dis = null;
            } catch (IOException e) {
                e.printStackTrace();
                logger.log(Level.INFO, e.toString());
            } finally {
                try {
                    if (null != dis) {
                        dis.close();
                    }
                    if (null != os) {
                        os.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    logger.log(Level.INFO, e.toString());
                }
            }
        }else{
            return ResponseDataTool.getTool().returnContent("not find file");
        }

        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 模糊查询
     * */
    @ApiOperation(value = "模糊查询")
    @RequestMapping(value = "/findKnowledgeByKeyword",method = RequestMethod.GET)
    public HashMap findKnowledgeByFilter(HttpServletRequest request,
                                         @ApiParam(name="keyword",value="关键字",required=false) String keyword,
                                         @ApiParam(name="typeCode",value="类型code",required=false) String typeCode,
                                         @ApiParam(name="indexPage",value="分页下标",required=true) String indexPage,
                                         @ApiParam(name="pageSize",value="查询单页大小",required=true) String pageSize) {
        logger.log(Level.INFO,"keyword:"+keyword+" knowType:"+typeCode);
        String userid = (String) request.getSession().getAttribute("userId");
        if (indexPage == null || pageSize == null || indexPage.isEmpty() || pageSize.isEmpty()) {
            indexPage = "1";
            pageSize = "10";
        }
        Sort sort = new Sort(Sort.Direction.DESC, "create_time");
        Pageable pageable = new PageRequest(Integer.parseInt(indexPage)-1,Integer.parseInt(pageSize),sort);
        return knowledgeService.findKnowledgeByKey(keyword,typeCode,userid,pageable);
    }

    /**
     * 资产查询查询
     * */
    @ApiOperation(value = "资产查询")
    @RequestMapping(value = "/findDeviceByKeyword",method = RequestMethod.GET)
    public HashMap findDeviceByKeyword(@ApiParam(name = "keyword",value = "关键字",required = false) String keyword) {
        HashMap map = new HashMap();
        String url = env.getProperty("interface.device.url");
        if (url == null) {
            map.put("message","interface device url is null");
            return ResponseDataTool.getTool().returnContent(map);
        }
        String resp = HttpUtil.getInstance().doGet(url+"/"+keyword);
        if (resp == null) {
            logger.log(Level.INFO, url + "返回空数据");
            map.put("message","response is null");
            return ResponseDataTool.getTool().returnContent(map);
        }
        JSONObject json = null;
        try {
            json = JSONObject.parseObject(URLDecoder.decode(resp,"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (json == null) {
            logger.log(Level.INFO, url + "返回数据无法解析:" + resp);
            map.put("message","response can't decode it");
            return ResponseDataTool.getTool().returnContent(map);
        }
        if (!json.getBooleanValue("success")) {
            logger.log(Level.INFO,url + "返回数据提示失败:" + resp);
            map.put("message","query is failed");
            return ResponseDataTool.getTool().returnContent(map);
        }
        map.put("data",json);
        return map;
    }


}
