package com.yl.knowledgelibray.config;

import com.yl.knowledgelibray.common.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.*;

import java.io.File;

/**
 * Created by luowanglin
 * Date: 2018/10/4
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class ApplicationConfig extends WebMvcConfigurerAdapter {

    @Autowired
    UserConfig userConfig;
    @Autowired
    Environment env;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        String os = System.getProperty("os.name");
//        if(os.toLowerCase().startsWith("win")){
//            System.out.println("That is windows system");
//            String paths = env.getProperty("file.upload.path");
//            registry.addResourceHandler("/file/**").addResourceLocations("file:"+paths);
//            System.out.println("*********路径测试："+paths);
//        }else{
//            System.out.println("That is linux or unix system");
//            registry.addResourceHandler("/file/**").addResourceLocations("file:"+env.getProperty("file.upload.path"));
//        }

    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //过滤前端BrowserRouter路径
        registry.addInterceptor(userConfig).addPathPatterns("/api/**").excludePathPatterns("/preview","/edit","/add","/file","/add/attachpstc","/edit/attachpstc");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("*")
                .allowedOrigins("*")
                .allowedHeaders("*")
                .exposedHeaders("Content-Disposition");
        super.addCorsMappings(registry);
    }

    @Bean
    public FileUtil initFileUtil() {
        return new FileUtil();
    }
}
