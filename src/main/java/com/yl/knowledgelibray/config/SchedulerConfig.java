package com.yl.knowledgelibray.config;

import com.yl.knowledgelibray.common.FileUtil;
import com.yl.knowledgelibray.controller.KnowledgelibrayController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by luowanglin
 * Date: 2018/10/19
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */
@Component
public class SchedulerConfig {

    @Autowired
    private FileUtil fileUtil;
    @Autowired
    private Environment env;

    /**
     * 定时清理临时文件夹
     * */
    @Scheduled(cron ="0 59 23 * * ?")
    public void cleanTempFileScheduled(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        System.out.println(df.format(new Date())+"delete file!.........");
        Path path = Paths.get(env.getProperty("file.upload.path")+KnowledgelibrayController.uploadTempDirectory);
        fileUtil.deleteFolder(path,true);
    }

}
