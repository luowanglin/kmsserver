package com.yl.knowledgelibray.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yl.knowledgelibray.common.HttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by luowanglin
 * Date: 2018/10/11
 * Time: 09:21
 * To change this template use File | Settings | File Templates.
 */
@Component
public class UserConfig implements HandlerInterceptor {

    @Autowired
    private Environment env;

    Logger logger = Logger.getLogger(UserConfig.class.getName());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String tokenPara = request.getParameter("token");
        if (tokenPara == null) {
            tokenPara = "";
        }
        HttpSession session = request.getSession();
        String sessionToken = (String) session.getAttribute("token");
        logger.log(Level.INFO,sessionToken+" ---- "+tokenPara);
        if ((sessionToken == null) || (tokenPara.equals(sessionToken) == false)) {
            session.setAttribute("token",tokenPara);
            logger.log(Level.INFO,"session token is null"+tokenPara);
            Map user = getUserId("/"+tokenPara);
            if (user == null || user.get("userId") == null) {
                logger.log(Level.INFO,"get user id is error");
                response.reset();
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Access-Control-Allow-Origin", "*");
                response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
                response.setHeader("Access-Control-Allow-Methods","POST, GET, DELETE,PUT");
                response.setHeader("Access-Control-Allow-Credentials","true"); //是否支持cookie跨域
                response.setContentType("application/json;charset=UTF-8");
                PrintWriter pw = response.getWriter();
                Map error = new HashMap();
                error.put("data",null);
                error.put("msg","user login failed,please log in again");
                error.put("success",false);
                error.put("code",401);
                pw.write(JSONObject.toJSONString(error));
                pw.flush();
                pw.close();
                return false;
            }
            session.setAttribute("userId",user.get("userId"));
            session.setAttribute("userName",user.get("userName"));
//            session.setAttribute("userId","6427fa45305d477cac3fa690291b9b6c");
//            session.setAttribute("userName","dev_test");
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        //
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        //
    }

    public Map<String,Object> getUserId(String token) throws Exception {
        String url = env.getProperty("interface.userinfo.url");
        if (url == null) {
            return null;
        }
        //通过http请求调用Api接口
        String resp = HttpUtil.getInstance().doGet(url+token);
        if (resp == null) {
            logger.log(Level.INFO,"同步userinfo失败！" + url + "返回空数据");
            return null;
        }
        JSONObject json = JSONObject.parseObject(resp);
        if (json == null) {
            logger.log(Level.INFO,"同步userinfo失败！" + url + "返回数据无法解析:" + resp);
            return null;
        }
        if (!json.getBooleanValue("success")) {
            logger.log(Level.INFO,"同步userinfo失败！" + url + "返回数据提示失败:" + resp);
            return null;
        }
        Map<String,Object> map = new HashMap<>();
        map.put("userId",json.getJSONObject("data").getString("user_id"));
        map.put("userName",json.getJSONObject("data").getString("user_name"));
        return map;
    }

}
