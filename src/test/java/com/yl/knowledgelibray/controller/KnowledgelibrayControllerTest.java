package com.yl.knowledgelibray.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by luowanglin
 * Date: 2018/9/23
 * Time: 10:51
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class KnowledgelibrayControllerTest {

    Logger logger = Logger.getLogger(KnowledgelibrayControllerTest.class.getName());

    @Autowired
    KnowledgelibrayController knowledgelibrayController;

    /**
     * 查找所有的知识类型
     */
    @Test
    public void findAllTypesTest() {
//        HashMap entity = knowledgelibrayController.getAllKnowledgeTypes();
//        logger.log(Level.INFO, JSON.toJSONString(entity));
    }

    /**
     * 通过用户信息获取用户id
     * */
    @Test
    public void getUserIdTest() {
//        HashMap entity = knowledgelibrayController.getUserId("dev","dev");
//        logger.log(Level.WARNING,JSON.toJSONString(entity));
    }

    /**
     * 获取所有知识条目
     * */
    @Test
    public void getAllKnowledgesTest() {
//        HashMap entity = knowledgelibrayController.getAllKnowledge("402880856610a75a016610a774d10000");
//        logger.log(Level.WARNING,JSON.toJSONString(entity));
    }

    /**
     * 获取指定类型的知识条目
     * */
    @Test
    public void getKnowledgesByTypeCodeTest() {
//        HashMap entity = knowledgelibrayController.getKnowledgeByTypecode(3000,"1","10");
//        logger.log(Level.WARNING,JSON.toJSONString(entity));
    }

    /**
     * 添加知识条目
     * */
    @Test
    public void addKnowledgeTest() {
//        Knowledge knowledge = new Knowledge();
//        knowledge.setCreatName("");
//        knowledge.setUserId("40288347661a413201661a4159050009");
//        knowledge.setPlan("test");
//        knowledge.setKnowType("PSTC");
//        HashMap map = knowledgelibrayController.addKnowledge(knowledge,null);
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 查找知识条目内容
     * */
    @Test
    public void findKnowledgeByKnowledgeIdTest() {
//        HashMap map = knowledgelibrayController.getKnowledgeContent("40288262665cbfe001665cc733b60001");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 删除知识条目
     * */
    @Test
    public void deleteKnowledgeTest() {
//        knowledgelibrayController.deleteKnowledgeById("402880856611c08d016611c0af560000");
    }


    /**
     * 修改知识条目
     * */
    @Test
    public void updataKnowledgeTest() {
//        knowledgelibrayController.updataKnowledge("402880856611c08d016611c0af560000","402880856610a75a016610a774d10000",
//                "test","PDCA","8989898","....test....",
//                "....test....","....test.....","....test....","....Test...",
//                null,"2018/09/20",null);
    }

    /**
     * 新增运维分类
     * */
    @Test
    public void addOPSCategoryTest() {
//        HashMap map = knowledgelibrayController.addOpsCategory("...test1...","风险评估","40288326661388eb016613890e350000");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 更新运维分类
     * */
    @Test
    public void updataOpsCategoryTest() {
//        HashMap map = knowledgelibrayController.updataOpsCategory("4028832666139e370166139e4a280000","40288326661388eb016613890e350000","风险评估","...ceshi..");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 查找所有运维分类
     * */
    @Test
    public void findAllOpsCategoryTest() {
//        HashMap map = knowledgelibrayController.findAllOpsCategory("40288326661388eb016613890e350000");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 删除运维分类
     * */
    @Test
    public void deleteOpsCategoryTest() {
//        HashMap map = knowledgelibrayController.deleteOpsCategory("4028832666139e370166139e4a280000");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 添加运维内容
     * */
    @Test
    public void addOpsContentTest() {
//        HashMap map = knowledgelibrayController.addCategoryContent(1,"4028832666139e370166139e4a280000","总结测试完成","总结","http://localhost");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 批量增加运维内容
     * */
    @Test
    public void addAbbatchOpsContentTest() {
//        List<ProcessCategoryContent> lists = new LinkedList<>();
//        for (int i = 0; i < 2; i++) {
//            ProcessCategoryContent content = new ProcessCategoryContent();
//            content.setAttachLink("http://localhost/test"+i);
//            content.setTitle("ceshi " + i);
//            content.setProcessType("ceshi.."+i);
//            content.setNum(i);
//            content.setProcessId("4028832666139e370166139e4a280000");
//            lists.add(content);
//        }
//        HashMap map = knowledgelibrayController.addBatchCategoryContent(lists);
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 查找运维内容
     * */
    @Test
    public void findAllOpsContentTest() {
//        HashMap map = knowledgelibrayController.findCategoryContetn("4028832666139e370166139e4a280000");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 更新运维内容
     * */
    @Test
    public void updataOpsContentTest() {
//        HashMap map = knowledgelibrayController.updataCategoryContent("40288347661a0e2c01661a0e41970000",1,"4028832666139e370166139e4a280000","zongjieceshi wancheng","zongjie","http://localhost:/lll");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 删除运维内容
     * */
    @Test
    public void deleteOpsContentTest() {
//        HashMap map = knowledgelibrayController.deleteCategoryContent("40288347661a0e2c01661a0e41970000");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 查询用户下的所有计划
     * */
    @Test
    public void findAllPlans() {
//        HashMap map = knowledgelibrayController.findAllPlans("40288347661a413201661a4159050009");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 获取计划内容
     * */
    @Test
    public void getPlanContent() {
//        HashMap map = knowledgelibrayController.findPlanContent("89898998");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 模糊查询
     * */
    @Test
    public void similarFindKnowledge() {
//        HashMap map = knowledgelibrayController.findKnowledgeByFilter("大");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }

    /**
     * 修改知识类型
     * */
    @Test
    public void updateKnowledgeTypeTest() {
//        KnowledgeType kt = new KnowledgeType();
//        kt.setTypeCode("4028808766256b1c0166256b39cc0000");
//        kt.setKnowledgeType("运行维护流程");
//        kt.setModifi(false);
//        HashMap hashMap = knowledgelibrayController.updateKnowledgeType(kt);
//        logger.log(Level.WARNING,JSON.toJSONString(hashMap));
    }

    /**
     * 资产查询
     * */
    @Test
    public void findDeviceByKeywordTest() {
//        HashMap map = knowledgelibrayController.findDeviceByKeyword("USB");
//        logger.log(Level.INFO, JSON.toJSONString(map));
    }

    /**
     * 获取知识父类型
     * */
    @Test
    public void findKnowledgeTypeByTypeCodeTest() {
//        HashMap map = knowledgelibrayController.findRootTypeByTypeCode(3001);
//        logger.log(Level.INFO,JSON.toJSONString(map));
    }

    /**
     * 获取特定类型的所有知识
     * */
    @Test
    public void findKnowldegesKnowTyepTest() {
//        HashMap map = knowledgelibrayController.findAllKnowledgeByKnowType(null,null,"PSTC");
//        logger.log(Level.WARNING,JSON.toJSONString(map));
    }
}
