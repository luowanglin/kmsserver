package com.yl.knowledgelibray.service;

import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.domain.PSTCPlanProcess;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by luowanglin
 * Date: 2018/9/23
 * Time: 23:23
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PSTCPlanProcessServiceTest {

    Logger logger = Logger.getLogger(PSTCPlanProcessServiceTest.class.getName());

    @Autowired
    PSTCPlanProcessService pstcPlanProcessService;

    /**
     * 添加PSTC计划处理事务
     * */
    @Test
    public void insertPlanTest() {
//        PSTCPlanProcess pstc = new PSTCPlanProcess();
//        pstc.setCompleteStand("ceshi...");
//        pstc.setOpsSchedule("ceshi...");
//        pstc.setpAdmin("ceshi...");
//        pstc.setPlanCode(1);
//        pstc.setProces("ceshi...");
//        pstc.setRender("ceshi...");
//        pstc.setResult("ceshi...");
//        pstc.setKnowledgeId("2c948a826618a838016618a8523b0000");
//        pstc.setSeqNum(1);
//        pstc.settFinishTime("2018/10/01 12:00:00");
//        List<PSTCPlanProcess> datas = new LinkedList<PSTCPlanProcess>();
//        datas.add(pstc);
//        pstcPlanProcessService.addAPSTCPlanBatchProcess(datas);
    }

    /**
     * 修改PSTC计划条目
     * */
    @Test
    public void updataPlanTest() {
//        PSTCPlanProcess pstc = new PSTCPlanProcess();
//        pstc.setId(1);
//        pstc.setCompleteStand("ceshi...");
//        pstc.setOpsSchedule("ceshi...");
//        pstc.setpAdmin("ceshi...");
//        pstc.setPlanCode(1);
//        pstc.setProces("ceshi...");
//        pstc.setRender("ceshi...");
//        pstc.setResult("ceshi...");
//        pstc.setKnowledgeId("402880856611c08d016611c0af560000");
//        pstc.setSeqNum(1);
//        pstc.settFinishTime("2018/10/01 12:00:00");
//        pstcPlanProcessService.updataPSTCPlanProcess(pstc);
    }


    /**
     * 查找PSTC所有条目
     **/
    @Test
    public void findAllPlanTest() {
//        List<PSTCPlanProcess> list = pstcPlanProcessService.findAllPlanProcessByKnowledgeId("402880856611c08d016611c0af560000");
//        if (list != null && list.size() > -1) {
//            for (PSTCPlanProcess pps: list) {
//                logger.log(Level.WARNING, JSON.toJSONString(pps));
//            }
//        }
    }


    /**
     * 删除PSTC指定条目
     * */
    @Test
    public void deletePlanByIdTest() {
//        pstcPlanProcessService.deletePSTCPlanProcessById(2);
    }

    /**
     * 获取所有的执行计划
     * */
    @Test
    public void findAllPlanProcessTest() {
//        List<PSTCPlanProcess> list = pstcPlanProcessService.findAllPlanProcessByKnowledgeId(264);
//        logger.log(Level.WARNING,JSON.toJSONString(list));
    }

}
