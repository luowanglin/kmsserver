package com.yl.knowledgelibray.service;

import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.domain.Device;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by luowanglin
 * Date: 2018/10/10
 * Time: 10:48
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DeviceServiceTest {

    Logger logger = Logger.getLogger(DeviceServiceTest.class.getName());

    @Autowired
    DeviceService deviceService;

    @Test
    public void addDeviceTest() {
//        Device device = new Device();
//        device.setDeviceName("testDevice");
//        Knowledge knowledge = new Knowledge();
//        device.setKnowledge(knowledge);
//        Device device1 = deviceService.addDevice(device);
//        logger.log(Level.INFO,JSON.toJSONString(device1));
    }

    @Test
    public void deleteDeviceTest() {
//        Knowledge knowledge = new Knowledge();
//        knowledge.setKnowledgeId("40288262665d0e8a01665d0f111d0000");
//        knowledge.setUserId("40288347661a413201661a4159050009");
//        deviceService.deleteAllDeviceByKnowledge(knowledge);
    }

    @Test
    public void findAllDeviceTest() {
//        List<Device> list = deviceService.findDevicesByKnowledgeId(44);
//        logger.log(Level.INFO,JSON.toJSONString(list));
    }

    @Test
    public void updataDeviceTest() {
//        Device device = new Device();
//        device.setKnowledgeId(288);
//        device.setDeviceName("防火墙-HUAWEI USG6650-LR201503003544");
//        device.setId(337);
//        deviceService.updateDeviceBy(device);
    }

    @Test
    public void deleteAllByKnowledgeIdTest() {
//        deviceService.deleteAllDeviceByKnowledge(288);
    }

}
