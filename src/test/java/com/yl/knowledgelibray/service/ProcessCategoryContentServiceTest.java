package com.yl.knowledgelibray.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by luowanglin
 * Date: 2018/9/26
 * Time: 10:04
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProcessCategoryContentServiceTest {

    Logger logger = Logger.getLogger(ProcessCategoryContentServiceTest.class.getName());

    @Autowired
    ProcessCategoryContentService pccs;

    /**
     * 添加过程内容
     * */
    @Test
    public void addCategoryContentTest() {
//        ProcessCategoryContent content = new ProcessCategoryContent();
//        content.setAttachLink("....test....");
//        content.setNum(1);
//        content.setProcessId("4028832666139e370166139e4a280000");
//        content.setProcessType(".....testtype......");
//        content.setTitle("...ttitle.....");
//        List<ProcessCategoryContent> data = new LinkedList<ProcessCategoryContent>();
//        data.add(content);
//        pccs.addCategoryContent(data);
    }

    /**
     * 更新过程内容
     * */
    @Test
    public void updataCategoryContentTest() {
//        ProcessCategoryContent content = new ProcessCategoryContent();
//        content.setContentId("402883266613bac0016613bad3de0000");
//        content.setAttachLink("....test....");
//        content.setNum(1);
//        content.setProcessId("4028832666139e370166139e4a280000");
//        content.setProcessType(".....testtype......");
//        content.setTitle("...updata title.....");
//        pccs.updataCategoryContent(content);
    }

    /**
     * 查找所有过程内容
     * */
    @Test
    public void findAllCategoryContentTest() {
//        List<ProcessCategoryContent> list = pccs.findCategoryContentByProcessId("4028832666139e370166139e4a280000");
//        logger.log(Level.WARNING,JSON.toJSONString(list));
    }

    /**
     * 删除过程内容
     * */
    @Test
    public void deleteCategoryContentTest() {
//        pccs.deleteCategoryContentByContentId("402883266613bac0016613bad3de0000");
    }


    /**
     * 查找AttachLink
     * */
    @Test
    public void findAttachLink() {
//        String attachlink = pccs.findAttachLinkByContentId(123);
//        logger.log(Level.INFO,attachlink);
    }


}
