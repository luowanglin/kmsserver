package com.yl.knowledgelibray.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.logging.Logger;

/**
 * Created by luowanglin
 * Date: 2018/9/26
 * Time: 09:34
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProcessCategoryServiceTest {

    Logger logger = Logger.getLogger(ProcessCategoryServiceTest.class.getName());

    @Autowired
    ProcessCategoryService processCategoryService;

    /**
     * 添加过程分类
     * */
    @Test
    public void addCategoryTest() {
//        ProcessCategory pc = new ProcessCategory();
//        pc.setDescription("...test...");
//        pc.setKnowledgeId("40288326661388eb016613890e350000");
//        pc.setProType("风险评估");
//        processCategoryService.addProcessCategory(pc);
    }

    /**
     * 修改过程分类
     * */
     @Test
    public void updataCategoryTest() {
//         ProcessCategory pc = new ProcessCategory();
//         pc.setDescription("...test...");
//         pc.setProcessId("4028832666139e370166139e4a280000");
//         pc.setKnowledgeId("40288326661388eb016613890e350000");
//         pc.setProType("前提计划");
//         processCategoryService.addProcessCategory(pc);
     }

    /**
     * 查找所有过程分类
     * */
    @Test
    public void findAllCategoryTest() {
//        List<ProcessCategory> list = processCategoryService.findProcessCategoryByKnowledgeId("40288326661388eb016613890e350000");
//        if (list != null && list.size() > -1) {
//            for (ProcessCategory pc: list) {
//                logger.log(Level.WARNING, JSON.toJSONString(pc));
//            }
//        }
    }

    /**
     * 删除过程分类
     * */
    @Test
    public void deleteCategoryTest() {
//       processCategoryService.deleteProcessCategoryByProcessId("4028832666139e370166139e4a280000");
    }

}
