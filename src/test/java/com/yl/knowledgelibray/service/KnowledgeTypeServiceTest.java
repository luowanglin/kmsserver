package com.yl.knowledgelibray.service;

import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.domain.KnowledgeType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/22
 * Time: 23:06
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class KnowledgeTypeServiceTest {

    @Autowired
    KnowledgeTypeService knowledgeTypeService;

    @Test
    public void updateKnowledgeTypeTest() {
//        KnowledgeType knowledgeType = new KnowledgeType();
//        knowledgeType.setKnowledgeType("PSTC");
//        knowledgeType.setTypeCode("40288087662568ae01662568c90f0000");
//        knowledgeType.setModifi(false);
//        knowledgeTypeService.updateType(knowledgeType);
    }

    @Test
    public void insertKnowledgeTypeTest() {
//        KnowledgeType knowledgeType = new KnowledgeType();
//        knowledgeType.setKnowledgeType("应急操作（EOP）");
//        knowledgeType.setFatherCode("4028808766256b1c0166256b39cc0000");
//        knowledgeType.setModifi(false);
//        knowledgeTypeService.addType(knowledgeType);
    }

    @Test
    public void deleteBatchTypeTest() {
//        List<Integer> list = new LinkedList<>();
//        list.add(1001);
//        list.add(1002);
//        list.add(1003);
//        list.add(2002);
//        list.add(2003);
//        knowledgeTypeService.deleteBatchKnowledgeType(list);
    }

    @Test
    public void findKnowledgeTypeTest() {
//        KnowledgeType kt = knowledgeTypeService.findKnowledgeTypeByTypeCode(3001);
//        System.out.println(JSON.toJSONString(kt));
    }

    @Test
    public void findRootTypeTest() {
//        String type = knowledgeTypeService.findRootType(3003);
//        System.out.println("类型："+type);
    }

    @Test
    public void updataKnowledgeTypeTest() {
//        KnowledgeType kt = new KnowledgeType();
//        kt.setKnowledgeType("bendi ceshi");
//        kt.setTypeCode(3172);
//        knowledgeTypeService.updateType(kt);
    }

    @Test
    public void findAllTypeCodesTest() {
//        List<Integer> list = knowledgeTypeService.findAllTypeCodeByTypeKnow("rwqerwqer");
//        System.out.println("类型Code集合："+JSON.toJSONString(list));
    }
}
