package com.yl.knowledgelibray.service;

import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.domain.Knowledge;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 21:21
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class KnowledgeServiceTest {

    Logger logger = Logger.getLogger(KnowledgeServiceTest.class.getName());

    @Autowired
    KnowledgeService knowledgeService;

    @Test
    public void addKnowledgeTest() {
//        Knowledge knowledge = new Knowledge();
//        knowledge.setCheckout("..PSTC..");
//        knowledge.setTime("..PSTC...");
//        knowledge.setDescription("...test....");
//        knowledge.setDoSomething("...test...");
//        knowledge.setImprovementAction("....test....");
//        knowledge.setKnowType("PSTC");
//        knowledge.setUserId("402880856616ab81016616ac29c20000");
//        List<Device> set = new LinkedList<>();
//        Device device = new Device();
//        device.setKnowledge(knowledge);
//        device.setDeviceName("testDevice1");
//        set.add(device);
//        Device device1 = new Device();
//        device1.setKnowledge(knowledge);
//        device1.setDeviceName("testDevice2");
//        set.add(device1);
//        knowledge.setDevices(set);
//        Knowledge obj = knowledgeService.addKnowledge(knowledge);
//        logger.log(Level.WARNING,JSON.toJSONString(obj)+"....");
    }

    @Test
    public void findKnowledgeTest() {
//        List<Knowledge> lists = knowledgeService.findAllKnowledge("40288347661a413201661a4159050009");
//        logger.log(Level.WARNING, JSON.toJSONString(lists));
    }

    @Test
    public void findKnowledgeByTypeTest() {
//        List<Knowledge> lists = knowledgeService.findKnowledgeByType("PDCA","402880856616ab81016616ac29c20000");
//        for (Knowledge obj:lists) {
//            logger.log(Level.WARNING,obj.toString());
//        }
    }

    @Test
    public void findKnowledgeByIdTest() {
//        Knowledge knowledge = knowledgeService.findKnowledgeById("40288262665cbfe001665cc733b60001");
//        logger.log(Level.INFO,"knowledge知识:"+JSON.toJSONString(knowledge));
    }

    @Test
    public void findAllByKeyword() {
//        Pageable pageable = new PageRequest(0,100);
//        HashMap hashMap = knowledgeService.findKnowledgeByKey("维护操作测试",null,"6427fa45305d477cac3fa690291b9b6c",pageable);
//        logger.log(Level.WARNING,"模糊查询："+JSON.toJSONString(hashMap));
    }

    @Test
    public void updateKnowledge() {
//        Knowledge knowledge = new Knowledge();
//        knowledge.setKnowledgeId("40288262665bfa7801665bfa8fd50000");
//        knowledge.setDescription("PSTC测试1");
//        List<Device> set = new LinkedList<>();
//        Device device = new Device();
//        device.setKnowledge(knowledge);
//        device.setDeviceName("testDevice3");
////        device.setId(4);
//        set.add(device);
//        Device device1 = new Device();
//        device1.setKnowledge(knowledge);
////        device1.setId(5);
//        device1.setDeviceName("testDevice4");
//        set.add(device1);
//        knowledge.setDevices(set);
//        logger.log(Level.INFO,JSON.toJSONString(knowledgeService.updataKnowledge(knowledge)));
    }

    @Test
    public void deleteKnowledge() {
        knowledgeService.deleteKnowledgeByKnowledgeId(243);
    }

    @Test
    public void findKnowledgeIdsTest() {
//        List<Integer> ids = knowledgeService.findKnowledgeIdsByTypeCode(3003);
//        logger.log(Level.INFO, "JSON:"+JSON.toJSONString(ids));
    }

    @Test
    public void findAllKnowledgeByTypeKnowTest() {
//        List<Knowledge> list = knowledgeService.findKnowledgeByTypeKnow("PDCA");
//        logger.log(Level.INFO,JSON.toJSONString(list));
    }

    @Test
    public void findKnowledgeTitleTest() {
//        String title = knowledgeService.findKnowledgeTitle(265);
//        logger.log(Level.WARNING,"标题："+title);
    }

}
